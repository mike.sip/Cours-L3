affiche([]).
affiche([X|R]) :- write(X), nl, affiche(R).

affiche2([]).
affiche2([X|R]) :- affiche2(R), nl, write(X).

premier1([X|R],X).

premier2([X|R]) :- write(X), nl.

dernier1([X],X).
dernier1([L|R],X) :- dernier1(R,X).

dernier2([X]) :- write(X), nl.
dernier2([L|R]) :- dernier2(R).

element(X,[X|R]).
element(X,[L|R]) :- element(X,R).

compte([],0).
compte([L|R],N) :- compte(R,N1), N is N1 + 1, N > 0.

somme([],0).
somme([X|R],N) :- somme(R,N1), N is N1 + X.

nieme(1,[X|R],X) :- !.
nieme(N,[L|R],X) :- N1 is N - 1, nieme(N1,R,X).

occurrence([],X,0).
occurrence([X|L],X,N) :- occurrence(L,X,N1), N is N1+1.
occurrence([Y|L],X,N) :- X\==Y, occurrence(L,X,N).