homme(andre).
    homme(bernard).
        homme(clement).
            homme(dudulle).
            homme(damien).
    homme(baptiste).
        homme(cedric).
            homme(didier).
            homme(dagobert).
            homme(dominique).
    homme(babar).



femme(augustine).
    femme(becassine).
        femme(chantal).
            femme(daniela).
        femme(celestine).
    femme(brigitte).
        femme(charlotte).
        femme(caroline).



    
    enfant(bernard, andre).
    enfant(bernard, augustine).
        enfant(clement, bernard).
        enfant(clement, becassine).  
            enfant(dudulle, clement).
            enfant(dudulle, chantal).  
            enfant(damien, clement).
            enfant(damien, chantal).
            enfant(daniela, clement).
            enfant(daniela, chantal).
        enfant(celestine, bernard).
        enfant(celestine, becassine).
    enfant(brigitte, andre).
    enfant(brigitte, augustine).      
        enfant(cedric, brigitte).
        enfant(cedric, baptiste). 
            enfant(didier, cedric).
            enfant(didier, charlotte).
            enfant(dagobert, cedric).
            enfant(dagobert, charlotte).
            enfant(dominique, cedric).
            enfant(dominique, charlotte).
        enfant(caroline, brigitte).
        enfant(caroline, baptiste).   
    enfant(babar, andre).
    enfant(babar, augustine).

parent(X,Y) :- enfant(Y,X).

fils(X,Y) :- enfant(X,Y),homme(X).
fille(X,Y) :- enfant(X,Y),femme(X).
pere(X,Y) :- parent(X,Y),homme(X).
mere(X,Y) :- parent(X,Y),femme(X).

grand_parent(X,Y) :- parent(X,Z),enfant(Y,Z).
petit_enfant(X,Y) :- grand_parent(Y,X).
grand_pere(X,Y) :- grand_parent(X,Y),homme(X).
grand_mere(X,Y) :- grand_parent(X,Y),femme(X).
petit_fils(X,Y) :- petit_enfant(X,Y),homme(X).
petite_fille(X,Y) :- petit_enfant(X,Y),femme(X).

frere_ou_soeur(X,Y) :- parent(Z,X),parent(Z,Y).
frere(X,Y) :- frere_ou_soeur(X,Y),homme(X).
soeur(X,Y) :- frere_ou_soeur(X,Y),femme(X).

oncle_ou_tante(X,Y) :- parent(Z,Y),frere_ou_soeur(X,Z).
oncle(X,Y) :- oncle_ou_tante(X,Y),homme(X).
femme(X,Y) :- oncle_ou_tante(X,Y),femme(X).

cousin_ou_cousine(X,Y) :- oncle_ou_tante(Z,X),enfant(Y,Z).
cousin(X,Y) :- cousin_ou_cousine(X,Y),homme(X).
cousine(X,Y) :- cousin_ou_cousine(X,Y),femme(X).

ancetre(X,Y) :- enfant(Y,Z).
ancetre(X,Y) :- enfant(Z,X),ancetre(Z,Y).

descendant(X,Y) :- parent(Y,X).
descendant(X,Y) :- parent(Z,X),descendant(Z,Y).