import java.util.ArrayList;
import java.util.List;

public abstract class ObservableDetecteur {

    private List<ObserverDetection> abonnes = new ArrayList<>();

    public enum TypeCapteur {CONTACT, INFRA, DOPPLER}

    public void notifierIntrusion(String type) {
        for (ObserverDetection a : abonnes) {
            a.notificationIntrusion(type);
        }
    }

    public void notifierFinIntrusion(String type) {
        for (ObserverDetection a : abonnes) {
            a.notificationFinIntrusion(type);
        }
    }

    public void addAbonne(ObserverDetection a) {
        abonnes.add(a);
    }

    public void delAbonne(ObserverDetection a) {
        abonnes.remove(a);
    }

    public abstract String getType();
}
