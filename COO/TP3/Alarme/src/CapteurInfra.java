import java.util.ArrayList;

public class CapteurInfra extends ObservableDetecteur {

    public void ondeDetectee() {
        System.out.println("Le capteur Infra a détecté quelque chose.");
        notifierIntrusion(this.getType());
    }

    public void ondeFin() {
        System.out.println("Le capteur Infra ne détecte plus rien.");
        notifierFinIntrusion(this.getType());
    }

    @Override
    public String getType() {
        return TypeCapteur.INFRA.toString();
    }
}
