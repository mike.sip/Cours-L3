import java.util.ArrayList;
import java.util.List;

public abstract class ObservableAlarme {
    private List<ObserverAlarme> abonnes = new ArrayList<>();

    public void notifierAlarme() {
        for (ObserverAlarme a : abonnes) {
            a.notificationAlarme();
        }
    }

    public void notifierFinAlarme() {
        for (ObserverAlarme a : abonnes) {
            a.notificationFinAlarme();
        }
    }

    public void addAbonnes(ObserverAlarme a) {
        abonnes.add(a);
    }

    public void delAbonne(ObserverAlarme a) {
        abonnes.remove(a);
    }
}
