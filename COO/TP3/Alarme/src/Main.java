public class Main {
    public static void main(String[] args) {
        ObservableDetecteur capteur_doppler = new CapteurDoppler();
        ObservableDetecteur capteur_contact = new CapteurContact();

        ControleurPiece garage = new ControleurPiece();
        capteur_doppler.addAbonne(garage);
        capteur_contact.addAbonne(garage);

        ObserverAlarme a1 = new SignalVisuel();
        ObserverAlarme a2 = new SignalSonore();

        garage.addAbonnes(a1);
        garage.addAbonnes(a2);

        ((CapteurDoppler) capteur_doppler).sonDetecte();
        ((CapteurContact) capteur_contact).contactDetecte();
    }
}
