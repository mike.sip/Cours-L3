import java.util.ArrayList;

public class CapteurDoppler extends ObservableDetecteur {

    public void sonDetecte() {
        System.out.println("Le capteur Son a détecté quelque chose.");
        notifierIntrusion(this.getType());
    }

    public void finSon() {
        System.out.println("Le capteur Son ne détecte plus rien.");
        notifierFinIntrusion(this.getType());
    }

    @Override
    public String getType() {
        return TypeCapteur.DOPPLER.toString();
    }
}
