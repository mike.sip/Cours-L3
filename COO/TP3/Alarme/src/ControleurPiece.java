import java.util.HashMap;
import java.util.Map;

public class ControleurPiece extends ObservableAlarme implements ObserverDetection {
    private Map<String, Integer> typeCapteur = new HashMap<>();

    public static int nbIntrusions = 2;

    @Override
    public void notificationIntrusion(String type) {
        if (!typeCapteur.containsKey(type)) {
            typeCapteur.put(type, 1);
        } else {
            typeCapteur.put(type, typeCapteur.get(type) + 1);
        }

        int somme = 0;

        for (String k : typeCapteur.keySet()) {
            somme += typeCapteur.get(k);
        }

        if (somme >= nbIntrusions) {
            System.out.println("Intrusion de type " + type + " détectée.");
            notifierAlarme();
        }
    }

    @Override
    public void notificationFinIntrusion(String type) {
        typeCapteur.put(type, typeCapteur.get(type) - 1);

        if (typeCapteur.get(type) < nbIntrusions) {
            System.out.println("Intrusion de type " + type + " terminée.");
            notifierFinAlarme();
        }

        if (typeCapteur.get(type) == 0) {
            typeCapteur.remove(type);
        }
    }
}
