
public class SignalVisuel implements ObserverAlarme {

    public void allumeLumiere() {
        System.out.println("Un signal visuel a été déclenché.");
    }

    public void eteintLumiere() {
        System.out.println("Un signal visual a été éteint");
    }

    @Override
    public void notificationAlarme() {
        System.out.println("ALERTE VISUEL !");
        allumeLumiere();
    }

    @Override
    public void notificationFinAlarme() {
        System.out.println("FIN ALERTE VISUEL");
        eteintLumiere();
    }
}
