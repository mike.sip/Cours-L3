import java.util.ArrayList;

public class CapteurContact extends ObservableDetecteur {

    public void contactDetecte() {
        System.out.println("Le capteur Contact a détecté quelque chose.");
        notifierIntrusion(this.getType());
    }

    public void finContact() {
        System.out.println("Le capteur Contact ne détecte plus rien.");
        notifierFinIntrusion(this.getType());
    }

    @Override
    public String getType() {
        return TypeCapteur.CONTACT.toString();
    }
}
