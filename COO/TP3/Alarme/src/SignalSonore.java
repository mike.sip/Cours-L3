
public class SignalSonore implements ObserverAlarme {

    public void declencheSon() {
        System.out.println("Un signal sonore a été déclenché.");
    }

    public void arreteSon() {
        System.out.println("Un signal sonore a été arrété.");
    }

    @Override
    public void notificationAlarme() {
        System.out.println("ALERTE SONORE !");
        declencheSon();
    }

    @Override
    public void notificationFinAlarme() {
        System.out.println("FIN ALERTE SONORE !");
        arreteSon();
    }
}
