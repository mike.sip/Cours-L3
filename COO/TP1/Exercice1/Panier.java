import java.util.List;

public class Panier {
    private String nom;
    private TypePaiement typePaiement;
    private List<Corrige> achats;

    public Panier(String nom) {
        this.nom = nom;
    }

    public float calculTotal() {
        float res = 0;
        for (Corrige corrige : this.achats) {
            res += corrige.getPrix();
        }
        return res;
    }

    public void effectuerPaiement() {
        this.typePaiement.payer();
        System.out.println("Vous venez d'effectuer le paiement pour le panier " + nom + " pour un montant de " + this.calculTotal() + " €.");
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public TypePaiement getTypePaiement() {
        return typePaiement;
    }

    public void setTypePaiement(TypePaiement typePaiement) {
        this.typePaiement = typePaiement;
    }

    public List<Corrige> getAchats() {
        return achats;
    }

    public void setAchats(List<Corrige> achats) {
        this.achats = achats;
    }
}