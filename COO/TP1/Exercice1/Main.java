import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args) throws InterruptedException {

        Panier monPanier = null;
        Scanner scanner = null;

        do {
            System.out.println(ANSI_RED + "Bienvenue !" + ANSI_RESET);
            System.out.println("------------------------------------");
            Thread.sleep(500);
            System.out.println(ANSI_PURPLE + "Phase de création de votre panier." + ANSI_RESET);
            System.out.println("------------------------------------");
            System.out.println(ANSI_CYAN + "Veuillez entrer le nom de votre panier." + ANSI_RESET);
            System.out.print("Votre réponse : ");

            scanner = new Scanner(System.in);
            monPanier = new Panier(scanner.nextLine());
        } while (monPanier == null);

        System.out.println("-> Traitement en cours");
        Thread.sleep(500);
        System.out.println("-> Opération réussie");
        Thread.sleep(500);

        System.out.println("------------------------------------");
        System.out.println(ANSI_PURPLE + "Phase d'ajout de corrigés dans votre panier." + ANSI_RESET);

        int response1;
        List<Corrige> corriges = new ArrayList<>();

        do {
            System.out.println("------------------------------------");
            System.out.println(ANSI_CYAN + "Veuillez choisir l'action à effectuer en tapant le numéro correspondant :"
                    + ANSI_RESET);
            System.out.println("1. Ajouter un corrigé.");
            if (corriges.size() > 0) {
                System.out.println("2. Passer à la phase de paiement.");
                System.out.println("3. Afficher le contenu de mon panier.");
            }
            System.out.print("Votre réponse : ");

            scanner = new Scanner(System.in);
            response1 = scanner.nextInt();

            if (response1 == 1) {
                System.out.println("------------------------------------");
                System.out.println(ANSI_YELLOW + "Ajouter un corrigé." + ANSI_RESET);
                System.out.println("------------------------------------");
                Thread.sleep(500);

                String nom;
                float prix;

                System.out.print(ANSI_CYAN + "Veuillez saisir le nom du corrigé : " + ANSI_RESET);
                scanner = new Scanner(System.in);
                nom = scanner.nextLine();

                System.out.println("");

                System.out.print(ANSI_CYAN + "Veuillez saisir le prix du corrigé : " + ANSI_RESET);
                scanner = new Scanner(System.in);
                prix = scanner.nextFloat();

                Thread.sleep(1000);

                Corrige corrige = new Corrige(prix, nom);
                corriges.add(corrige);
                System.out.println("------------------------------------");
                System.out.println(ANSI_GREEN + "Corrigé ajouté avec succès !" + ANSI_RESET);
            }

            if (response1 == 3) {
                System.out.println("------------------------------------");
                System.out.println(ANSI_YELLOW + "Contenu du panier." + ANSI_RESET);
                System.out.println("------------------------------------");
                Thread.sleep(500);

                System.out.println(corriges);
            }
        } while (response1 != 2);

        monPanier.setAchats(corriges);
        Thread.sleep(500);

        System.out.println("------------------------------------");
        System.out.println(ANSI_PURPLE + "Phase de paiement." + ANSI_RESET);

        int response2;
        TypePaiement typePaiement = null;

        do {
            System.out.println("------------------------------------");
            System.out.println(ANSI_CYAN + "Veuillez choisir une action :" + ANSI_RESET);
            System.out.println("1. Carte bancaire.");
            System.out.println("2. PayPal.");
            System.out.println("3. Afficher le montant total du panier.");
            if (typePaiement != null) {
                System.out.println("4. Effectuer le paiement.");
                System.out
                        .println("Vous pouvez modifier votre mode de paiement en sélectionnant de nouveau une action.");
            }
            System.out.print("Votre réponse : ");

            scanner = new Scanner(System.in);
            response2 = scanner.nextInt();

            if (response2 == 1) {
                System.out.println("------------------------------------");
                System.out.println(ANSI_YELLOW + "Paiement par carte bancaire." + ANSI_RESET);
                System.out.println("------------------------------------");
                Thread.sleep(500);

                String nomPorteur;

                System.out
                        .print(ANSI_CYAN + "Veuillez saisir le nom du titulaire de la carte bancaire : " + ANSI_RESET);
                scanner = new Scanner(System.in);
                nomPorteur = scanner.nextLine();

                Thread.sleep(1000);

                typePaiement = new CB(nomPorteur);
                monPanier.setTypePaiement(typePaiement);
                System.out.println("------------------------------------");
                System.out.println(ANSI_GREEN + "Carte bancaire ajouté avec succès !" + ANSI_RESET);
            }

            if (response2 == 2) {
                System.out.println("------------------------------------");
                System.out.println(ANSI_YELLOW + "Paiement par PayPal." + ANSI_RESET);
                System.out.println("------------------------------------");
                Thread.sleep(500);

                String email;
                int numAutorisation;

                System.out.print(ANSI_CYAN + "Veuillez saisir l'adresse email du compte Paypal : " + ANSI_RESET);
                scanner = new Scanner(System.in);
                email = scanner.nextLine();

                System.out
                        .print(ANSI_CYAN + "Veuillez saisir le numéro d'autorisation du compte Paypal : " + ANSI_RESET);
                scanner = new Scanner(System.in);
                numAutorisation = scanner.nextInt();

                typePaiement = new Paypal(email, numAutorisation);
                monPanier.setTypePaiement(typePaiement);
                System.out.println("------------------------------------");
                System.out.println(ANSI_GREEN + "Compte PayPal ajouté avec succès !" + ANSI_RESET);
            }
            if (response2 == 3) {
                System.out.println("------------------------------------");
                System.out.println(ANSI_YELLOW + "Montant total du panier." + ANSI_RESET);
                System.out.println("------------------------------------");
                Thread.sleep(500);

                System.out.println(monPanier.calculTotal() + " €");
            }
        } while (response2 != 4);

        System.out.println("------------------------------------");
        System.out.println(ANSI_RED + "Procédure de paiement en cours !" + ANSI_RESET);
        System.out.println("------------------------------------");
        System.out.print(".");
        Thread.sleep(1000);
        System.out.print(".");
        Thread.sleep(1000);
        System.out.println(".");
        System.out.println("------------------------------------");
        monPanier.effectuerPaiement();
        System.out.println(ANSI_BLUE + "Au revoir !" + ANSI_RESET);
    }
}