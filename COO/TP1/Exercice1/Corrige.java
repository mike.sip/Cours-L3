public class Corrige {
    private String nom;
    private float prix;

    public Corrige(float prix, String nom) {
        this.nom = nom;
        this.prix = prix;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String toString() {
        return "{" + this.nom + " : " + this.prix + "}";
    }
}
