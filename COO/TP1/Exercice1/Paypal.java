public class Paypal implements TypePaiement {
    private String email;
    private int numAutorisation;

    public Paypal(String email, int numAutorisation) {
        this.email = email;
        this.numAutorisation = numAutorisation;
    }

    @Override
    public void payer() {
        System.out.println("Vous avez payé avec le compte Paypal de " + email);

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNumAutorisation() {
        return numAutorisation;
    }

    public void setNumAutorisation(int numAutorisation) {
        this.numAutorisation = numAutorisation;
    }
}
