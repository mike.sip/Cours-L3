public class CB implements TypePaiement {
    private String nomPorteur;

    public CB(String nomPorteur) {
        this.nomPorteur = nomPorteur;
    }

    @Override
    public void payer() {
        System.out.println("Vous avez payé avec la CB de " + nomPorteur);
    }

    public String getNomPorteur() {
        return nomPorteur;
    }

    public void setNomPorteur(String nomPorteur) {
        this.nomPorteur = nomPorteur;
    }

}
