public class DeplacementBB8 implements TypeDeplacement {

    @Override
    public float avance(float pos, boolean estAccidente) {
        return (estAccidente) ? pos + 2 : pos + 5;
    }
    
}
