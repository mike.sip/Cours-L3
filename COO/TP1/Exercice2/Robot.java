public class Robot {
    private String nom;
    private float position;
    private TypeDeplacement typeDeplacement;

    public Robot(String nom) {
        this.nom = nom;
        this.position = 0;
    }

    public void deplacer(Iterrain terrain) {

    }

    public TypeDeplacement getTypeDeplacement() {
        return typeDeplacement;
    }

    public void setTypeDeplacement(TypeDeplacement typeDeplacement) {
        this.typeDeplacement = typeDeplacement;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public float getPosition() {
        return position;
    }

    public void setPosition(float position) {
        this.position = position;
    }

}
