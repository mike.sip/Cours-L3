import java.util.ArrayList;
import java.util.List;

public class Zoo {
    private List<Animal> mesAnimaux;

    public Zoo() {
        this.mesAnimaux = new ArrayList<>();
    }

    public void nourrirAnimaux() {
        for (Animal a : this.mesAnimaux) {
            a.nourrir();
        }
    }

    public void addAnimal(Animal a) {
        this.mesAnimaux.add(a);
    }

    public void blesse(String nom) {
        for (Animal a : this.mesAnimaux) {
            if (a.getNom().equals(nom)) {
                a.setMonHabitat(new Cage());
            }
        }
    }

    public void liberte(String nom) {
        for (Animal a : this.mesAnimaux) {
            if (a.getNom().equals(nom)) {
                a.setMonHabitat(new Libre());
            }
        }
    }

}
