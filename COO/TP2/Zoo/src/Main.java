import java.util.*;

public class Main {
    public static void main(String[] args) {
        Zoo Z = new Zoo();

        FabriqueAnimal F1 = new Normal();

        Z.addAnimal(F1.creerAnimal(Normal.TYPE.PERROQUET, "Ara"));
        Z.addAnimal(F1.creerAnimal(Normal.TYPE.PERROQUET, "Alex"));
        Z.addAnimal(F1.creerAnimal(Normal.TYPE.CHIEN, "Nestor"));
        Z.addAnimal(F1.creerAnimal(Normal.TYPE.CHIEN, "Pluto"));
        Z.addAnimal(F1.creerAnimal(Normal.TYPE.LION, "Simba"));
        Z.addAnimal(F1.creerAnimal(Normal.TYPE.LION, "Sarabi"));

        Z.nourrirAnimaux();

        Z.liberte("Simba");
        Z.blesse("Nestor");
        Z.nourrirAnimaux();

        /*FabriqueAnimal F2 = new Clone();

        Z.addAnimal(F2.creerAnimal(Normal.TYPE.PERROQUET, "Ami"));
        Z.addAnimal(F2.creerAnimal(Normal.TYPE.PERROQUET, "Ami"));
        Z.addAnimal(F2.creerAnimal(Normal.TYPE.CHIEN, "Ami"));
        Z.addAnimal(F2.creerAnimal(Normal.TYPE.CHIEN, "Ami"));


        Z.nourrirAnimaux();*/

    }
}
