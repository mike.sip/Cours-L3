
public class Animal {
    private String nom;

    private Habitat monHabitat;

    public Animal(String nom, Habitat monHabitat) {
        this.nom = nom;
        this.monHabitat = monHabitat;
    }

    public void nourrir() {
        this.monHabitat.manger();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Habitat getMonHabitat() {
        return monHabitat;
    }

    public void setMonHabitat(Habitat monHabitat) {
        this.monHabitat = monHabitat;
    }
}
