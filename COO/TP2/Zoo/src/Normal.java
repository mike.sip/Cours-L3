
public class Normal implements FabriqueAnimal {

    public static enum TYPE {CHIEN, LION, PERROQUET}

    public Normal() {
    }

    @Override
    public Animal creerAnimal(TYPE type, String nom) {
        Animal animal;
        switch (type) {
            case CHIEN:
                animal = new Chien(nom);
                break;
            case LION:
                animal = new Lion(nom);
                break;
            case PERROQUET:
                animal = new Perroquet(nom);
                break;
            default:
                System.out.println("Type inexistant");
                return null;
        }
        return animal;
    }
}
