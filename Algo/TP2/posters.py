# import time

# n = 25 # nombre de posters affichés dans les couloirs
# # temps de lecture et enrichissement de chaque poster (t[k],e[k] sont les info pour le poster k)
# t = [16, 15, 26, 36, 12, 13, 35, 25, 22, 7, 13, 15, 25, 14, 26, 17, 25, 13, 12, 11, 22, 33, 15, 13, 15]
# e = [15, 9, 7, 18, 14, 15, 16, 18, 8, 14, 8, 11, 8, 3, 9, 15, 19, 1, 2, 3, 3, 13, 13, 14, 2]
# m = 70 # nombre de minutes avant le rendez-vous

def ResolPoster(t,e,i,tps):
    if  tps <= 0 or i >= len(t):
        return 0
    else:
        ei1 = 0
        if t[i] <= tps:
            ei1 = e[i] + ResolPoster(t, e, i + 1, tps - t[i])
        ei2 = ResolPoster(t, e, i + 1, tps)

        return max(ei1, ei2)

print(ResolPoster(t, e, 0, 70))

# def poster(n,t,e,m):
#     start = time.perf_counter()
#     opt = [[0 for tps in range(m + 1)] for i in range(n + 1)]
#     for tps in range(m + 1):
#         opt[0][tps] = 0

#     for i in range(n + 1):
#         opt[i][0] = 0

#     for i in range(n + 1):
#         for tps in range(m + 1):
#             ei1, ei2 = 0, 0
#             if t[i - 1] <= tps:
#                 ei1 = e[i - 1] + opt[i - 1][tps - t[i - 1]]
#             ei2 = opt[i - 1][tps]
#             opt[i][tps] = max(ei1, ei2)
#     end = time.perf_counter()
#     print("Temps d'exécution: ", end - start)
#     return opt[n][m]

# print(poster(n,t,e,m))

# def posterContructif(n,t,e,m):
#     # créer un tableau de taille (n+1)x(m+1) initialisé à 0 :
#     Opt = [[ (0,[]) for tps in range(m+1)] for i in range(n+1)]
    
#     # pour i=0
#     for tps in range(m+1):
#         Opt[0][tps] = (0,[])
#     # pour tps=0
#     for i in range(n+1):
#         Opt[i][0] = (0,[])
    
#     for i in range(n+1):
#         for tps in range(m+1):
#             if tps>=t[i-1]:
#                 cas1 = e[i-1] + Opt[i-1][tps-t[i-1]][0]
#                 cas2 = Opt[i-1][tps][0]
#                 if cas1 > cas2:
#                     Opt[i][tps] = (cas1, Opt[i-1][tps-t[i-1]][1]+[i])
#                 else:
#                     Opt[i][tps] = (cas2, Opt[i-1][tps][1])
#             else:
#                 cas2 = Opt[i-1][tps]
#                 Opt[i][tps] = cas2
    
#     return Opt[n][m] # <- prob. initial, avec n posters et temps m

# print(posterContructif(n,t,e,m))

M=[1,0,1,1,1,1,1,0,1,0,0,1,1]

def Set4True(M, i):
    n = len(M)
    M[min(i, n)] = 1
    M[min(i + 1, n)] = 1
    M[min(i + 2, n)] = 1
    M[min(i + 3, n)] = 1

def Set1True(M, i):
    n = len(M)
    M[min(i, n)] = 1

def SetAllTrue(M):
    i = 0
    while i < len(M):
        if M[i] == 0:
            Set4True(M, i)
            i += 3
        else:
            i += 1

def SetAllTruePenalite(M):
    penal = [0]
    for i in range(len(M)):
        if M[i] == 0:
            cpt = 1
            for j in range(i + 1, min(len(M), i + 3) + 1):
                if M[j] == 0:
                    cpt += 1
            if cpt > 2:
                Set4True(M, i)
                penal.append(2)
            else:
                Set1True(M, i)
                penal.append(1)
    return penal

print(SetAllTruePenalite(M))