#ifndef PROJET_VECTEUR_H
#define PROJET_VECTEUR_H

class Vecteur
{
private:
    int x;
    int y;

public:
    Vecteur();

    Vecteur(int x, int y);

    virtual ~Vecteur();

    int getX() const;

    void setX(int x);

    int getY() const;

    void setY(int y);
};

#endif // PROJET_VECTEUR_H
