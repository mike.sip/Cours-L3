#ifndef PROJET_COULEUR_H
#define PROJET_COULEUR_H

class Couleur
{
private:
    int r;
    int g;
    int b;

public:
    Couleur(int r, int g, int b);

    virtual ~Couleur();

    int getR() const;

    void setR(int r);

    int getG() const;

    void setG(int g);

    int getB() const;

    void setB(int b);
};

#endif // PROJET_COULEUR_H
