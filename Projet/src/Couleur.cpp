#include "Couleur.h"

Couleur::Couleur(int r, int g, int b) : r(r), g(g), b(b) {}

int Couleur::getR() const
{
    return r;
}

void Couleur::setR(int r)
{
    Couleur::r = r;
}

int Couleur::getG() const
{
    return g;
}

void Couleur::setG(int g)
{
    Couleur::g = g;
}

int Couleur::getB() const
{
    return b;
}

void Couleur::setB(int b)
{
    Couleur::b = b;
}

Couleur::~Couleur()
{
}
