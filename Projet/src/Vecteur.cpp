#include "Vecteur.h"

Vecteur::Vecteur() : x(0), y(0) {}

Vecteur::Vecteur(int x, int y) : x(x), y(y) {}

int Vecteur::getX() const
{
    return x;
}

void Vecteur::setX(int x)
{
    Vecteur::x = x;
}

int Vecteur::getY() const
{
    return y;
}

void Vecteur::setY(int y)
{
    Vecteur::y = y;
}

Vecteur::~Vecteur()
{
}
