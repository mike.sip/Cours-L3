#ifndef PROJET_TRAJECTOIRE_H
#define PROJET_TRAJECTOIRE_H

#include "vector"
#include "string"
#include "Circuit.h"
#include "Vecteur.h"
#include "png++-0.2.10/png.hpp"

class Trajectoire
{
private:
    std::vector<Vecteur> vecteurs;

public:
    Trajectoire();

    virtual ~Trajectoire();

    const std::vector<Vecteur> &getVecteurs() const;

    void setVecteurs(const std::vector<Vecteur> &vecteurs);

    void addVecteur(const Vecteur &vecteur);

    // FONCTION QUI PERMET DE CALCULER LES ETAPES (METHODE ESCARGOT)
    void calculer_escargot(const Circuit &circuit);

    // FONCTION QUI VERIFIE SI UNE ETAPE EST VALIDE (NE PASSE PAS PAR UN PIXEL NOIR)
    static bool estValide(const Circuit &circuit, const Vecteur &v1, const Vecteur &v2)
    {
        bool res = true;
        int x1, y1, x2, y2;
        x1 = v1.getX();
        y1 = v1.getY();
        x2 = v2.getX();
        y2 = v2.getY();

        int dx, dy, p;
        dx = x2 - x1;
        dy = y2 - y2;
        p = 2 * dy - dx;

        while (x1 <= x2)
        {
            if (p < 0)
            {
                x1 += 1;
                p += 2 * dy;
            }
            else
            {
                x1 += 1;
                y1 += 1;
                p += 2 * (dy - dx);
            }

            // VERIFIER SI PIXEL NOIR
            // SI PIXEL NOIR, RES = FALSE
            png::image<png::rgb_pixel> image(circuit.getPathFile());
            png::rgb_pixel pixel = image.get_pixel(x1, y1);

            int red = (int)pixel.red;
            int green = (int)pixel.green;
            int blue = (int)pixel.blue;

            if (red == 0 && green == 0 && blue == 0)
            {
                res = false;
            }
        }

        return res;
    }
};

#endif // PROJET_TRAJECTOIRE_H
