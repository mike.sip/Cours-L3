#include "Circuit.h"

Circuit::Circuit(const std::string &pathFile, int accMax, const Vecteur &depart, const Couleur &couleurArrivee)
    : path_file(pathFile), acc_max(accMax), depart(depart), couleur_arrivee(couleurArrivee) {}

Circuit::~Circuit()
{
}

const std::string &Circuit::getPathFile() const
{
    return path_file;
}

void Circuit::setPathFile(const std::string &pathFile)
{
    path_file = pathFile;
}

int Circuit::getAccMax() const
{
    return acc_max;
}

void Circuit::setAccMax(int accMax)
{
    acc_max = accMax;
}

const Vecteur &Circuit::getDepart() const
{
    return depart;
}

void Circuit::setDepart(const Vecteur &depart)
{
    Circuit::depart = depart;
}

const Couleur &Circuit::getCouleurArrivee() const
{
    return couleur_arrivee;
}

void Circuit::setCouleurArrivee(const Couleur &couleurArrivee)
{
    couleur_arrivee = couleurArrivee;
}
