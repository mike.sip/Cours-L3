#ifndef PROJET_CIRCUIT_H
#define PROJET_CIRCUIT_H

#include "string"
#include "Vecteur.h"
#include "Couleur.h"

class Circuit
{
private:
    std::string path_file;
    int acc_max;
    Vecteur depart;
    Couleur couleur_arrivee;

public:
    Circuit(const std::string &pathFile, int accMax, const Vecteur &depart, const Couleur &couleurArrivee);

    virtual ~Circuit();

    const std::string &getPathFile() const;

    void setPathFile(const std::string &pathFile);

    int getAccMax() const;

    void setAccMax(int accMax);

    const Vecteur &getDepart() const;

    void setDepart(const Vecteur &depart);

    const Couleur &getCouleurArrivee() const;

    void setCouleurArrivee(const Couleur &couleurArrivee);
};

#endif // PROJET_CIRCUIT_H
