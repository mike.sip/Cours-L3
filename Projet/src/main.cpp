#include <iostream>
#include "png++-0.2.10/png.hpp"
#include "toml11-master/toml.hpp"

int main()
{
    std::cout << "Hello, World!" << std::endl;

    // Test PNG++
    png::image<png::rgb_pixel> image(128, 128);
    for (png::uint_32 y = 0; y < image.get_height(); ++y)
    {
        for (png::uint_32 x = 0; x < image.get_width(); ++x)
        {
            image[y][x] = png::rgb_pixel(x, y, x + y);
            // non-checking equivalent of image.set_pixel(x, y, ...);
        }
    }
    image.write("rgb.png");

    // Test TOML
    auto data = toml::parse("circuits/CestToutDroit/basique.toml");
    int acc_max = toml::find<int>(data, "acc_max");

    std::cout << acc_max << std::endl;

    return 0;
}
