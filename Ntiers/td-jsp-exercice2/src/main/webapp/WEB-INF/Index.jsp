<%--
  Created by IntelliJ IDEA.
  User: o2193611
  Date: 14/01/2022
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="fragments/Header.jsp" %>
<main class="pt-3">
    <div class="mx-auto fit-content">
        <jsp:useBean id="user" scope="session" type="modele.Utilisateur"/>
        <h1>Bienvenue ${user.login} !</h1>
    </div>
    <div class="mx-auto fit-content">
        <img src="http://i.stack.imgur.com/SBv4T.gif" alt="" width="250"/>
    </div>
</main>
<%@ include file="fragments/Footer.jsp" %>