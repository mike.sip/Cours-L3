<%--
  Created by IntelliJ IDEA.
  User: o2193611
  Date: 14/01/2022
  Time: 11:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Connexion | UnivBet</title>
    <link href="https://zupimages.net/up/22/02/hc3f.png" rel="icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>
</head>
<style>
    .fit-content {
        width: fit-content;
    }

    .centered {
        position: fixed;
        top: 50%;
        left: 50%;
        /* bring your own prefixes */
        transform: translate(-50%, -50%);
    }
</style>
<body>
<main>
    <jsp:useBean id="error" scope="request" type="java.lang.String" class="java.lang.String"/>
    <c:if test="${!(error.equals(''))}">
        <c:choose>
            <c:when test="${error.equals('user')}">
                <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                    <strong>Erreur !</strong> Utilisateur déjà connecté.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </c:when>
            <c:otherwise>
                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                    <strong>Attention !</strong> Identifiants incorrects.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </c:otherwise>
        </c:choose>
    </c:if>
    <div class="shadow p-3 rounded centered">
        <div class="mx-auto mb-3 fit-content">
            <img src="https://zupimages.net/up/22/02/hc3f.png" alt="" width="250" height="250">
        </div>
        <form method="post" action="/univbet/connexion">
            <div class="mb-3">
                <input type="text" class="form-control" name="pseudo" placeholder="Pseudo">
            </div>
            <div class="mb-3">
                <input type="password" class="form-control" name="motdepasse" placeholder="Mot de passe">
            </div>
            <div class="mx-auto fit-content">
                <button type="submit" class="btn btn-primary">Se connecter</button>
            </div>
        </form>
    </div>
</main>
</body>
</html>
