<%--
  Created by IntelliJ IDEA.
  User: mikes
  Date: 15/01/2022
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ include file="../fragments/Header.jsp" %>
<main class="pt-3">
    <div class="mx-auto fit-content">
        <h1>Mes paris</h1>
    </div>
    <div class="container table-responsive py-3">
        <table class="table table-bordered table-striped table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">Sport</th>
                <th scope="col">Equipe 1</th>
                <th scope="col">Equipe 2</th>
                <th scope="col">Date</th>
                <th scope="col">Prédiction</th>
                <th scope="col">Mise</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <jsp:useBean id="mesparis" scope="request" type="java.util.Collection"/>
            <c:forEach items="${ mesparis }" var="paris">
                <fmt:parseDate var="date" value="${paris.match.quand}" pattern="yyyy-MM-dd'T'HH:mm"/>
                <tr>
                    <td>${paris.match.sport}</td>
                    <td>${paris.match.equipe1}</td>
                    <td>${paris.match.equipe2}</td>
                    <td>
                        <fmt:formatDate value="${date}" pattern="dd/MM/yyyy HH:mm"/>
                    </td>
                    <c:choose>
                        <c:when test="${!(paris.vainqueur.equals('nul'))}">
                            <td>Victoire de ${paris.vainqueur}</td>
                        </c:when>
                        <c:otherwise>
                            <td>Match nul</td>
                        </c:otherwise>
                    </c:choose>
                    <td>${paris.montant} €</td>
                    <td>
                        <jsp:useBean id="now" scope="request" type="java.util.Date"/>
                        <c:choose>
                            <c:when test="${date.after(now)}">
                                <form id="formPariAnnule${paris.idPari}" method="post" action="/univbet/mesparis">
                                    <input type="hidden" name="idPari" value="${paris.idPari}">
                                    <button type="submit" class="btn btn-danger" title="Annuler ce pari">
                                        <i class="far fa-window-close"></i>
                                    </button>
                                </form>
                            </c:when>
                            <c:otherwise>
                                Match terminé !
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <script>
                    $("document").ready(function () {
                        $("#formPariAnnule${paris.idPari}").submit(function (e) {
                            e.preventDefault();
                            $.ajax({
                                type: "post",
                                url: "/univbet/mesparis",
                                data: $(this).serialize(),
                                success: function () {
                                    Swal.fire({
                                        icon: "success",
                                        title: 'Pari annulé',
                                        timer: 1500,
                                        timerProgressBar: true,
                                        showConfirmButton: false
                                    }).then((result) => {
                                        if (result.dismiss === Swal.DismissReason.timer) {
                                            window.location.href = "/univbet/mesparis";
                                        }
                                    })
                                }
                            })
                        })
                    });
                </script>
            </c:forEach>
            </tbody>
        </table>
    </div>
</main>
<%@ include file="../fragments/Footer.jsp" %>
