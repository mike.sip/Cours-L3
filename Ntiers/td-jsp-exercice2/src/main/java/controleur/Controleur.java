package controleur;

import facade.FacadeParisStaticImpl;
import facade.exceptions.*;
import modele.Utilisateur;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "controleur", urlPatterns = "/univbet/*")
public class Controleur extends HttpServlet {
    private static final String CONNEXION = "connexion";
    private static final String DECONNEXION = "deconnexion";
    private static final String INDEX = "accueil";
    private static final String MATCHS = "matchs";
    private static final String MESPARIS = "mesparis";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] navigation = uri.split("/");
        String cleNavigation = navigation[navigation.length - 1];
        FacadeParisStaticImpl f = null;
        Utilisateur user = null;
        switch (cleNavigation) {
            case CONNEXION:
                this.getServletContext().getRequestDispatcher("/WEB-INF/Connexion.jsp").forward(req, resp);
                break;
            case DECONNEXION:
                f = (FacadeParisStaticImpl) this.getServletContext().getAttribute("facade");
                user = (Utilisateur) req.getSession().getAttribute("user");
                f.deconnexion(user.getLogin());
                req.getSession().invalidate();
                resp.sendRedirect("/univbet/connexion");
                break;
            case INDEX:
                req.setAttribute("titre", "Accueil");
                this.getServletContext()
                        .getRequestDispatcher("/WEB-INF/Index.jsp")
                        .forward(req, resp);
                break;
            case MATCHS:
                f = (FacadeParisStaticImpl) this.getServletContext().getAttribute("facade");
                req.setAttribute("now", new Date());
                req.setAttribute("matchs", f.getAllMatchs());
                req.setAttribute("titre", "Matchs");
                this.getServletContext()
                        .getRequestDispatcher("/WEB-INF/templates/Matchs.jsp")
                        .forward(req, resp);
                break;
            case MESPARIS:
                f = (FacadeParisStaticImpl) this.getServletContext().getAttribute("facade");
                user = (Utilisateur) req.getSession().getAttribute("user");
                req.setAttribute("now", new Date());
                req.setAttribute("mesparis", f.getMesParis(user.getLogin()));
                req.setAttribute("titre", "Mes paris");
                this.getServletContext()
                        .getRequestDispatcher("/WEB-INF/templates/MesParis.jsp")
                        .forward(req, resp);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] navigation = uri.split("/");
        String cleNavigation = navigation[navigation.length - 1];
        FacadeParisStaticImpl f = null;
        Utilisateur user = null;
        switch (cleNavigation) {
            case CONNEXION:
                f = (FacadeParisStaticImpl) this.getServletContext().getAttribute("facade");
                user = null;
                try {
                    user = f.connexion(req.getParameter("pseudo"), req.getParameter("motdepasse"));
                    req.getSession().setAttribute("user", user);
                    req.setAttribute("titre", "Accueil");
                    resp.sendRedirect("/univbet/accueil");
                } catch (UtilisateurDejaConnecteException e) {
                    req.setAttribute("error", "user");
                    this.getServletContext().getRequestDispatcher("/WEB-INF/Connexion.jsp").forward(req, resp);
                } catch (InformationsSaisiesIncoherentesException e) {
                    req.setAttribute("error", "info");
                    this.getServletContext().getRequestDispatcher("/WEB-INF/Connexion.jsp").forward(req, resp);
                }
                break;
            case DECONNEXION:
                break;
            case INDEX:
                break;
            case MATCHS:
                f = (FacadeParisStaticImpl) this.getServletContext().getAttribute("facade");
                user = (Utilisateur) req.getSession().getAttribute("user");
                try {
                    f.parier(user.getLogin(), Long.parseLong(req.getParameter("idMatch")), req.getParameter("vainqueur"), Double.parseDouble(req.getParameter("mise")));
                } catch (MatchClosException | ResultatImpossibleException | MontantNegatifOuNulException e) {
                    e.printStackTrace();
                }
                resp.sendRedirect("/univbet/matchs");
                break;
            case MESPARIS:
                f = (FacadeParisStaticImpl) this.getServletContext().getAttribute("facade");
                user = (Utilisateur) req.getSession().getAttribute("user");
                try {
                    f.annulerPari(user.getLogin(), Long.parseLong(req.getParameter("idPari")));
                } catch (OperationNonAuthoriseeException e) {
                    e.printStackTrace();
                }
                resp.sendRedirect("/univbet/mesparis");
                break;
        }
    }
}
