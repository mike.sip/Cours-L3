package listeners;

import facade.FacadeParisStaticImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class InitialisationFacade implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        FacadeParisStaticImpl f = new FacadeParisStaticImpl();
        sce.getServletContext().setAttribute("facade", f);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContextListener.super.contextDestroyed(sce);
    }
}
