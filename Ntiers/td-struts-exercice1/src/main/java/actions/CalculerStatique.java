package actions;

import com.opensymphony.xwork2.ActionSupport;

public class CalculerStatique extends ActionSupport {
    private double operande1;
    private String operation;
    private double operande2;
    private double resultat;

    @Override
    public String execute() throws Exception {
        switch (this.operation) {
            case "Somme":
                this.resultat = this.operande1 + this.operande2;
                break;
            case "Soustraction":
                this.resultat = this.operande1 - this.operande2;
                break;
            case "Division":
                if (this.operande2 == 0) {
                    this.addFieldError("operande2","L'opérande doit être supérieure à 0");
                    return INPUT;
                }
                else {
                    this.resultat = this.operande1 / this.operande2;
                }
                break;
            case "Multiplication":
                this.resultat = this.operande1 * this.operande2;
                break;
        }
        return SUCCESS;
    }

    public double getOperande1() {
        return operande1;
    }

    public void setOperande1(double operande1) {
        this.operande1 = operande1;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public double getOperande2() {
        return operande2;
    }

    public void setOperande2(double operande2) {
        this.operande2 = operande2;
    }

    public double getResultat() {
        return resultat;
    }

    public void setResultat(double resultat) {
        this.resultat = resultat;
    }
}
