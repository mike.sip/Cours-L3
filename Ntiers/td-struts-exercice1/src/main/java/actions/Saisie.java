package actions;

import com.opensymphony.xwork2.ActionSupport;

public class Saisie extends ActionSupport {
    private String pseudo;
    private String motdepasse;

    @Override
    public void validate() {
        if (this.pseudo.length() < 3) {
            this.addFieldError("pseudo", getText("erreur.pseudo"));
        }
        if (this.motdepasse.length() < 3) {
            this.addFieldError("motdepasse", getText("erreur.motdepasse"));
        }
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }
}
