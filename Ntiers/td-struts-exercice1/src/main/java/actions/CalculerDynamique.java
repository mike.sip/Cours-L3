package actions;

import com.opensymphony.xwork2.ActionSupport;
import modele.CalculatriceDynamiqueDuFuturImpl;
import org.apache.struts2.interceptor.ApplicationAware;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class CalculerDynamique extends ActionSupport implements ApplicationAware {
    private CalculatriceDynamiqueDuFuturImpl calculatriceDynamiqueDuFuturImpl;
    private Long nbUtilisation;
    private Collection<String> operations;
    private Double operande1;
    private String operation;
    private Double operande2;
    private Double resultat;

    @Override
    public String execute() throws Exception {
        this.nbUtilisation = this.calculatriceDynamiqueDuFuturImpl.getValeurCompteur();
        this.operations = this.calculatriceDynamiqueDuFuturImpl.getOperations();
        if (Objects.isNull(this.operande1) && Objects.isNull(this.operande2)) {
            return INPUT;
        }
        this.resultat = this.calculatriceDynamiqueDuFuturImpl.doCalcul(this.operation, this.operande1, this.operande2);
        return SUCCESS;
    }

    @Override
    public void setApplication(Map<String, Object> map) {
        this.calculatriceDynamiqueDuFuturImpl = (CalculatriceDynamiqueDuFuturImpl) map.get("calculatriceDynamiqueDuFuturImpl");
        if (Objects.isNull(this.calculatriceDynamiqueDuFuturImpl)) {
            this.calculatriceDynamiqueDuFuturImpl = new CalculatriceDynamiqueDuFuturImpl();
            map.put("calculatriceDynamiqueDuFuturImpl", this.calculatriceDynamiqueDuFuturImpl);
        }
    }

    public CalculatriceDynamiqueDuFuturImpl getCalculatriceDynamiqueDuFuturImpl() {
        return calculatriceDynamiqueDuFuturImpl;
    }

    public void setCalculatriceDynamiqueDuFuturImpl(CalculatriceDynamiqueDuFuturImpl calculatriceDynamiqueDuFuturImpl) {
        this.calculatriceDynamiqueDuFuturImpl = calculatriceDynamiqueDuFuturImpl;
    }

    public Long getNbUtilisation() {
        return nbUtilisation;
    }

    public void setNbUtilisation(Long nbUtilisation) {
        this.nbUtilisation = nbUtilisation;
    }

    public Collection<String> getOperations() {
        return operations;
    }

    public void setOperations(Collection<String> operations) {
        this.operations = operations;
    }

    public Double getOperande1() {
        return operande1;
    }

    public void setOperande1(Double operande1) {
        this.operande1 = operande1;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Double getOperande2() {
        return operande2;
    }

    public void setOperande2(Double operande2) {
        this.operande2 = operande2;
    }

    public Double getResultat() {
        return resultat;
    }

    public void setResultat(Double resultat) {
        this.resultat = resultat;
    }
}
