package actions;

import com.opensymphony.xwork2.ActionSupport;
import modele.Etudiant;
import modele.FacadeModele;
import org.apache.struts2.interceptor.ApplicationAware;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class ListeEtudiant extends ActionSupport implements ApplicationAware {
    private FacadeModele facadeModele;
    private Collection<Etudiant> etudiants;

    @Override
    public String execute() throws Exception {
        this.etudiants = this.facadeModele.getEtudiants();
        return SUCCESS;
    }

    @Override
    public void setApplication(Map<String, Object> map) {
        this.facadeModele = (FacadeModele) map.get("facadeModele");
        if (Objects.isNull(this.facadeModele)) {
            this.facadeModele = new FacadeModele();
            map.put("facadeModele", this.facadeModele);
        }
    }

    public FacadeModele getFacadeModele() {
        return facadeModele;
    }

    public void setFacadeModele(FacadeModele facadeModele) {
        this.facadeModele = facadeModele;
    }

    public Collection<Etudiant> getEtudiants() {
        return etudiants;
    }

    public void setEtudiants(Collection<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }
}
