<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Struts2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
</head>
<body>
<style>
    .fit-content {
        width: fit-content;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <s:a class="navbar-brand" action="accueil"><s:text name="navbar.accueil"/></s:a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <s:a class="nav-link" action="gotoconnexion"><s:text name="navbar.connexion"/></s:a>
                </li>
                <li class="nav-item">
                    <s:a class="nav-link" action="gotocalculatriceStatique"><s:text name="navbar.calculatrice.statique"/></s:a>
                </li>
                <li class="nav-item">
                    <s:a class="nav-link" action="gotocalculatriceDynamique"><s:text name="navbar.calculatrice.dynamique"/></s:a>
                </li>
                <li class="nav-item">
                    <s:a class="nav-link" action="gotolisteetudiant"><s:text name="navbar.listeetudiant"/></s:a>
                </li>
            </ul>
        </div>
    </div>
</nav>
