<%@include file="../fragments/header.jsp" %>
<main>
    <div class="mx-auto fit-content">
        <h1><s:text name="navbar.connexion"/></h1>
    </div>
    <div class="mx-auto fit-content">
        <s:fielderror theme="bootstrap"/>
        <s:form action="connexion" theme="bootstrap">
            <s:textfield class="form-control" name="pseudo" key="pseudo"/>
            <s:password class="form-control" name="motdepasse" key="motdepasse"/>
            <div class="mt-3 mx-auto fit-content">
                <s:submit class="btn btn-primary" key="bouton.seconnecter"/>
            </div>
        </s:form>
    </div>
</main>
<%@include file="../fragments/footer.jsp" %>

