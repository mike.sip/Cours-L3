<%@include file="../fragments/header.jsp" %>
<main>
    <jsp:useBean id="operande1" scope="request" type="java.lang.Double"/>
    <jsp:useBean id="operation" scope="request" type="java.lang.String"/>
    <jsp:useBean id="operande2" scope="request" type="java.lang.Double"/>
    <jsp:useBean id="resultat" scope="request" type="java.lang.Double"/>
    <div class="mx-auto fit-content">
        <h1><s:text name="resultat"/></h1>
    </div>
    <div class="mx-auto fit-content">
        <p>${operande1} ${operation} ${operande2} = ${resultat}</p>
    </div>
</main>
<%@include file="../fragments/footer.jsp" %>