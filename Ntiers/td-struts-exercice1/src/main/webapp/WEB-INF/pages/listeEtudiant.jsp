<%@include file="../fragments/header.jsp" %>
<main>
    <jsp:useBean id="etudiants" scope="request" type="java.util.Collection"/>
    <div class="mx-auto fit-content">
        <h1><s:text name="navbar.listeetudiant"/></h1>
    </div>
    <div class="mx-auto fit-content">
        <table class="table table-bordered table-hover">
            <thead class="table-dark">
            <th><s:text name="etudiant.num"/></th>
            <th><s:text name="etudiant.nom"/></th>
            <th><s:text name="etudiant.prenom"/></th>
            <th><s:text name="etudiant.age"/></th>
            </thead>
            <tbody>
            <s:iterator value="etudiants" var="etudiant">
                <tr>
                    <td>${etudiant.numeroEtudiant}</td>
                    <td>${etudiant.nom}</td>
                    <td>${etudiant.prenom}</td>
                    <td>${etudiant.age}</td>
                </tr>
            </s:iterator>
            </tbody>
        </table>
    </div>
</main>
<%@include file="../fragments/footer.jsp" %>