<%@include file="../fragments/header.jsp" %>
<main>
    <jsp:useBean id="nbUtilisation" scope="request" type="java.lang.Long"/>
    <jsp:useBean id="operations" scope="request" type="java.util.Collection"/>
    <div class="mx-auto fit-content text-center">
        <h1><s:text name="navbar.calculatrice.dynamique"/></h1>
        <h2><s:text name="compteurUtilisation"/>: ${nbUtilisation}</h2>
    </div>
    <div class="mx-auto fit-content">
        <s:form action="calculerdynamique" theme="bootstrap">
            <div class="d-flex">
                <s:textfield type="number" name="operande1"/>
                <s:select name="operation" list="operations"/>
                <s:textfield type="number" name="operande2"/>
            </div>
            <div class="mt-3 mx-auto fit-content">
                <s:submit class="btn btn-primary" key="bouton.calculer"/>
            </div>
        </s:form>
    </div>
</main>
<%@include file="../fragments/footer.jsp" %>

