<%@include file="../fragments/header.jsp" %>
<main>
    <div class="mx-auto fit-content">
        <h1><s:text name="navbar.calculatrice.statique"/></h1>
    </div>
    <div class="mx-auto fit-content">
        <s:form action="calculerstatique" theme="bootstrap">
            <div class="d-flex">
                <s:textfield type="number" name="operande1"/>
                <s:select name="operation" list="{'Somme','Soustraction','Division','Multiplication'}"/>
                <s:textfield type="number" name="operande2"/>
            </div>
            <div class="mt-3 mx-auto fit-content">
                <s:submit class="btn btn-primary" key="bouton.calculer"/>
            </div>
        </s:form>
    </div>
</main>
<%@include file="../fragments/footer.jsp" %>