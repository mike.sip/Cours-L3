<%@include file="../fragments/header.jsp" %>
<main>
    <jsp:useBean id="pseudo" scope="request" type="java.lang.String"/>
    <jsp:useBean id="motdepasse" scope="request" type="java.lang.String"/>
    <div class="mx-auto fit-content">
        <h1><s:text name="infosaisies"/></h1>
    </div>
    <div class="mx-auto fit-content w-25">
        <table class="table table-bordered">
            <thead class="table-dark">
            <th><s:text name="pseudo"/></th>
            <th><s:text name="motdepasse"/></th>
            </thead>
            <tbody>
            <td>${pseudo}</td>
            <td>${motdepasse}</td>
            </tbody>
        </table>
    </div>
</main>
<%@include file="../fragments/footer.jsp" %>