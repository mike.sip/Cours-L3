package controleur.ordres;

public enum TypeOrdre {
    SHOW_MENU,
    SHOW_CONSULTER,
    LOAD_FILMS,
    SHOW_AJOUT,
    LOAD_GENRES,
    AJOUT_FILM,
    ERREUR_FILM_GENRE,
    ERREUR_FILM_NOM
}
