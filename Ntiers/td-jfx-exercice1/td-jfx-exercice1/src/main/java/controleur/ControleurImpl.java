package controleur;

import controleur.abstractControleur.AbstractControleur;
import controleur.ordres.TypeOrdre;
import facadeCreaFilm.FacadeScreen;

import facadeCreaFilm.GenreNotFoundException;
import facadeCreaFilm.NomFilmDejaExistantException;
import javafx.stage.Stage;
import modeleCreaFilm.Film;
import vues.Ajout;
import vues.GestionnaireVue;
import vues.Menu;
import vues.TousLesFilms;

import java.util.Collection;
import java.util.List;

public class ControleurImpl extends AbstractControleur {

    public FacadeScreen facadeScreen;

    public ControleurImpl(GestionnaireVue gestionnaireVue, FacadeScreen facadeScreen, ControleurSetUp controleurSetUp) {
        super(gestionnaireVue);
        this.facadeScreen = facadeScreen;
        controleurSetUp.setUp(this, getGestionnaireVue());
    }

    @Override
    public void run() {
        this.fireOrdre(TypeOrdre.SHOW_MENU);
    }

    public void gotoConsulter() {
        this.fireOrdre(TypeOrdre.SHOW_CONSULTER);
        this.fireOrdre(TypeOrdre.LOAD_FILMS);
    }

    public void gotoMenu() {
        this.fireOrdre(TypeOrdre.SHOW_MENU);
    }

    public void gotoAjout() {
        this.fireOrdre(TypeOrdre.SHOW_AJOUT);
        this.fireOrdre(TypeOrdre.LOAD_GENRES);
    }

    public Collection<Film> getTousLesFilms() {
        return this.facadeScreen.getAllFilms();
    }

    public void creerFilm(String titre, String genre, String realisateur) {
        try {
            facadeScreen.creerFilm(titre, realisateur, genre);
            this.fireOrdre(TypeOrdre.SHOW_MENU);
        } catch (GenreNotFoundException e) {
            this.fireOrdre(TypeOrdre.ERREUR_FILM_GENRE);
        } catch (NomFilmDejaExistantException e) {
            this.fireOrdre(TypeOrdre.ERREUR_FILM_NOM);
        }
    }
}
