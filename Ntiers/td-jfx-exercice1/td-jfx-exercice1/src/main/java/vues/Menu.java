package vues;

import controleur.ControleurImpl;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import vues.abstractVues.AbstractVueInteractive;

public class Menu extends AbstractVueInteractive implements EcouteurOrdre {

    private BorderPane borderPane;
    private Button btnConsulter;
    private Button btnAjouter;

    public static Menu creerVue(GestionnaireVue gestionnaireVue) {
        Menu menu = new Menu();
        menu.initialiserVue();
        menu.initialisation();
        gestionnaireVue.ajouterVueInteractive(menu);
        gestionnaireVue.ajouterEcouteurOrdre(menu);
        return menu;
    }

    private void initialiserVue() {
        this.borderPane = new BorderPane();
        this.btnConsulter = new Button("Consulter");
        this.btnAjouter = new Button("Ajouter");

        this.btnConsulter.setMaxWidth(Double.MAX_VALUE);
        this.btnConsulter.setOnAction(e -> this.getControleur().gotoConsulter());
        this.btnAjouter.setMaxWidth(Double.MAX_VALUE);
        this.btnAjouter.setOnAction(e -> this.getControleur().gotoAjout());

        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(20);
        vBox.getChildren().addAll(this.btnConsulter, this.btnAjouter);

        this.borderPane.setCenter(vBox);

        Label label = new Label("Les films");
        label.setFont(Font.font(32));
        this.borderPane.setTop(label);

        BorderPane.setAlignment(label, Pos.CENTER);
        BorderPane.setAlignment(vBox, Pos.CENTER);
    }

    @Override
    public Parent getTopParent() {
        return this.borderPane;
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this, TypeOrdre.SHOW_CONSULTER, TypeOrdre.SHOW_AJOUT);
    }

    @Override
    public void traiter(TypeOrdre e) {

    }
}
