package vues;

import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import modeleCreaFilm.GenreFilm;
import vues.abstractVues.AbstractVueInteractive;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.List;

public class Ajout extends AbstractVueInteractive implements EcouteurOrdre {

    @FXML
    private BorderPane borderPane;
    @FXML
    private TextField titre;
    @FXML
    private ComboBox<GenreFilm> genre;
    @FXML
    private TextField realisateur;

    public static Ajout creerVue(GestionnaireVue gestionnaireVue) {
        URL location = Ajout.class.getResource("Ajout.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);

        try {
            fxmlLoader.load();

            Ajout vue = fxmlLoader.getController();
            vue.initialisation();
            gestionnaireVue.ajouterVueInteractive(vue);
            gestionnaireVue.ajouterEcouteurOrdre(vue);
            return vue;
        } catch (IOException e) {
            throw new RuntimeException("Problème FXML: Ajout.fxml");
        }
    }

    @Override
    public Parent getTopParent() {
        return this.borderPane;
    }

    public void chargerGenres(Collection<GenreFilm> lesGenres) {
        this.genre.getItems().clear();
        this.genre.getItems().addAll(lesGenres);
    }

    public void gotomenu(ActionEvent actionEvent) {
        this.getControleur().gotoMenu();
    }

    public void ajouterFilm(ActionEvent actionEvent) {
        String titre = this.titre.getText();
        String genre = String.valueOf(this.genre.getValue());
        String realisateur = this.realisateur.getText();

        if (titre.length() > 0 && genre.length() > 0 && realisateur.length() > 0) {
            this.getControleur().creerFilm(titre, genre, realisateur);
        } else {
            this.afficherErreur("Erreur d'argument", "Vous devez saisir tous les champs !");
        }
    }

    public void afficherErreur(String erreur, String s) {
        Alert alert = new Alert(Alert.AlertType.ERROR, s);
        alert.setTitle(erreur);
        alert.showAndWait();
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this, TypeOrdre.LOAD_GENRES, TypeOrdre.AJOUT_FILM, TypeOrdre.ERREUR_FILM_GENRE, TypeOrdre.ERREUR_FILM_NOM);
    }

    @Override
    public void traiter(TypeOrdre e) {
        if (e == TypeOrdre.LOAD_GENRES) {
            this.chargerGenres(List.of(GenreFilm.values()));
        }

        if (e == TypeOrdre.AJOUT_FILM) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Film ajouté");
            alert.setContentText("Le film a bien été enregistré");
            alert.showAndWait();
            this.titre.setText("");
            this.realisateur.setText("");
        }

        if (e == TypeOrdre.ERREUR_FILM_GENRE) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de genre ");
            alert.setContentText("Ce genre n'existe pas");
            alert.showAndWait();
        }

        if (e == TypeOrdre.ERREUR_FILM_NOM) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Conflit de noms");
            alert.setContentText("Un film existe déjà sous ce nom : " + this.titre.getText());
            alert.showAndWait();
        }
    }
}
