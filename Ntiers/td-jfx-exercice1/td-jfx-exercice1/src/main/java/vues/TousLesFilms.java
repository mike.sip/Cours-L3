package vues;

import controleur.ControleurImpl;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import modeleCreaFilm.Film;
import vues.abstractVues.AbstractVueInteractive;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

public class TousLesFilms extends AbstractVueInteractive implements EcouteurOrdre {

    @FXML
    private BorderPane borderPane;
    @FXML
    private ListView<Film> listeFilms;

    public static TousLesFilms creerVue(GestionnaireVue gestionnaireVue) {
        URL location = TousLesFilms.class.getResource("TousLesFilms.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);

        try {
            fxmlLoader.load();

            TousLesFilms vue = fxmlLoader.getController();
            vue.initialisation();
            gestionnaireVue.ajouterVueInteractive(vue);
            gestionnaireVue.ajouterEcouteurOrdre(vue);
            return vue;
        } catch (IOException e) {
            throw new RuntimeException("Problème FXML: TousLesFilms.fxml");
        }
    }

    public void chargerFilms(Collection<Film> lesFilms) {
        this.listeFilms.getItems().clear();
        this.listeFilms.getItems().addAll(lesFilms);
    }

    public void gotomenu(ActionEvent actionEvent) {
        this.getControleur().gotoMenu();
    }

    @Override
    public Parent getTopParent() {
        return this.borderPane;
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this, TypeOrdre.LOAD_FILMS);
    }

    @Override
    public void traiter(TypeOrdre e) {
        if (e == TypeOrdre.LOAD_FILMS) {
            Collection<Film> lesFilms = this.getControleur().facadeScreen.getAllFilms();
            this.chargerFilms(lesFilms);
        }
    }
}
