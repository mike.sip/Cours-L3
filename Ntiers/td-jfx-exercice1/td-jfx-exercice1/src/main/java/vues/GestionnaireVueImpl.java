package vues;

import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.stage.Stage;
import vues.abstractVues.AbstractGestionnaireVue;

public class GestionnaireVueImpl extends AbstractGestionnaireVue {
    private Menu menu;
    private Ajout ajout;
    private TousLesFilms tousLesFilms;

    public GestionnaireVueImpl(Stage stage) {
        super(stage);
        menu = Menu.creerVue(this);
        ajout = Ajout.creerVue(this);
        tousLesFilms = TousLesFilms.creerVue(this);
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this, TypeOrdre.SHOW_MENU, TypeOrdre.SHOW_CONSULTER, TypeOrdre.SHOW_AJOUT);
    }

    @Override
    public void traiter(TypeOrdre e) {
        switch (e) {
            case SHOW_MENU: {
                this.getStage().setScene(this.menu.getScene());
                this.getStage().show();
                break;
            }

            case SHOW_CONSULTER: {
                this.getStage().setScene(this.tousLesFilms.getScene());
                this.getStage().show();

                break;
            }

            case SHOW_AJOUT: {
                this.getStage().setScene(this.ajout.getScene());
                this.getStage().show();

                break;
            }

        }
    }
}
