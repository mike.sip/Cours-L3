module fr.univ.orleans.pnt {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    opens vues to javafx.fxml, javafx.graphics;

    opens modeleCreaFilm to javafx.base;
    opens facadeCreaFilm to javafx.base;
    exports pnt;
}