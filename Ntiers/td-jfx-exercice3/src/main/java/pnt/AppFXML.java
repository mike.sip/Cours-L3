package pnt;

import controleur.ControleurImpl;
import facadeCreaFilm.FacadeScreen;
import facadeCreaFilm.FacadeScreenImpl;
import javafx.application.Application;
import javafx.stage.Stage;
import vues.GestionnaireVueImpl;

import java.io.IOException;

public class AppFXML extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        GestionnaireVueImpl gestionnaireVue = new GestionnaireVueImpl(stage);
        FacadeScreen facadeModele = new FacadeScreenImpl();
        ControleurImpl controle = new ControleurImpl(gestionnaireVue, facadeModele);
        controle.run();
    }


    public static void main(String[] args) {
        launch();
    }
}