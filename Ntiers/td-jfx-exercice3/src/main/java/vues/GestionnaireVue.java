package vues;

import controleur.ordres.EcouteurOrdre;

public interface GestionnaireVue {
    void ajouterVueInteractive(VueInteractive vue);

    void ajouterEcouteurOrdre(EcouteurOrdre ecouteurOrdre);
}
