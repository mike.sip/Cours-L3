package actions;

import modele.Utilisateur;

public class Annuler extends Contexte {
    private long idPari;

    @Override
    public String execute() throws Exception {
        Utilisateur user = (Utilisateur) this.getSession().get("user");
        this.getF().annulerPari(user.getLogin(), this.idPari);
        return SUCCESS;
    }

    public long getIdPari() {
        return idPari;
    }

    public void setIdPari(long idPari) {
        this.idPari = idPari;
    }
}
