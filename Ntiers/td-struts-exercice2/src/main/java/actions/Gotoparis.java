package actions;

import modele.Pari;
import modele.Utilisateur;

import java.util.Collection;

public class Gotoparis extends Contexte {
    private Collection<Pari> mesparis;

    @Override
    public String execute() throws Exception {
        Utilisateur user = (Utilisateur) this.getSession().get("user");
        this.mesparis = this.getF().getMesParis(user.getLogin());
        return SUCCESS;
    }

    public Collection<Pari> getMesparis() {
        return mesparis;
    }

    public void setMesparis(Collection<Pari> mesparis) {
        this.mesparis = mesparis;
    }
}
