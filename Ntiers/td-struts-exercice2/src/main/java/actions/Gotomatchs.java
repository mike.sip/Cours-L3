package actions;

import modele.Match;

import java.time.LocalDateTime;
import java.util.Collection;

public class Gotomatchs extends Contexte {
    private LocalDateTime now;
    private Collection<Match> matchs;

    @Override
    public String execute() throws Exception {
        this.now = LocalDateTime.now();
        this.matchs = this.getF().getAllMatchs();
        return SUCCESS;
    }

    public LocalDateTime getNow() {
        return now;
    }

    public void setNow(LocalDateTime now) {
        this.now = now;
    }

    public Collection<Match> getMatchs() {
        return matchs;
    }

    public void setMatchs(Collection<Match> matchs) {
        this.matchs = matchs;
    }
}
