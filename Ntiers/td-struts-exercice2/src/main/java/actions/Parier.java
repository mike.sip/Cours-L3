package actions;

import modele.Utilisateur;

public class Parier extends Contexte {
    private String idMatch;
    private String vainqueur;
    private String mise;

    @Override
    public String execute() throws Exception {
        Utilisateur user = (Utilisateur) this.getSession().get("user");
        this.getF().parier(user.getLogin(), Long.parseLong(this.idMatch), this.vainqueur, Double.parseDouble(this.mise));
        return INPUT;
    }

    public String getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(String idMatch) {
        this.idMatch = idMatch;
    }

    public String getVainqueur() {
        return vainqueur;
    }

    public void setVainqueur(String vainqueur) {
        this.vainqueur = vainqueur;
    }

    public String getMise() {
        return mise;
    }

    public void setMise(String mise) {
        this.mise = mise;
    }
}
