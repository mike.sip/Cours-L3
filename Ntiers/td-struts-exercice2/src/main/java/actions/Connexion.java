package actions;

import facade.exceptions.InformationsSaisiesIncoherentesException;
import facade.exceptions.UtilisateurDejaConnecteException;
import modele.Utilisateur;

public class Connexion extends Contexte {
    private String pseudo;
    private String motdepasse;
    private String error;

    @Override
    public String execute() throws Exception {
        try {
            Utilisateur user = this.getF().connexion(this.pseudo, this.motdepasse);
            this.getSession().put("user", user);
        } catch (UtilisateurDejaConnecteException e) {
            this.error = "user";
            return ERROR;
        } catch (InformationsSaisiesIncoherentesException e) {
            this.error = "info";
            return ERROR;
        }
        return INPUT;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
