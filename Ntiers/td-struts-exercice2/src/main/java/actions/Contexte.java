package actions;

import com.opensymphony.xwork2.ActionSupport;
import facade.FacadeParisStaticImpl;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;
import java.util.Objects;

public abstract class Contexte extends ActionSupport implements ApplicationAware, SessionAware {
    private FacadeParisStaticImpl f;
    private Map<String, Object> session;

    @Override
    public void setApplication(Map<String, Object> map) {
        this.f = (FacadeParisStaticImpl) map.get("f");
        if (Objects.isNull(this.f)) {
            this.f = new FacadeParisStaticImpl();
            map.put("f", this.f);
        }
    }

    public FacadeParisStaticImpl getF() {
        return f;
    }

    public void setF(FacadeParisStaticImpl f) {
        this.f = f;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }
}
