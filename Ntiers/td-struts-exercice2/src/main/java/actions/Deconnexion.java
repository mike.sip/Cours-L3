package actions;

import modele.Utilisateur;

public class Deconnexion extends Contexte {
    @Override
    public String execute() throws Exception {
        this.getF().deconnexion(((Utilisateur)this.getSession().get("user")).getLogin());
        this.getSession().clear();
        return SUCCESS;
    }
}
