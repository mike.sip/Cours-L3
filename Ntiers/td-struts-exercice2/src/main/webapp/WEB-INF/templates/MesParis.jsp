<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: mikes
  Date: 15/01/2022
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../fragments/Header.jsp" %>
<main class="pt-3">
    <div class="mx-auto fit-content">
        <h1>Mes paris</h1>
    </div>
    <div class="container table-responsive py-3">
        <table class="table table-bordered table-striped table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">Sport</th>
                <th scope="col">Equipe 1</th>
                <th scope="col">Equipe 2</th>
                <th scope="col">Date</th>
                <th scope="col">Prédiction</th>
                <th scope="col">Mise</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <s:iterator value="mesparis" var="pari">
                <tr>
                    <td><s:property value="#pari.match.sport"/></td>
                    <td><s:property value="#pari.match.equipe1"/></td>
                    <td><s:property value="#pari.match.equipe2"/></td>
                    <td>
                        <fmt:parseDate var="date" value="${pari.match.quand}" pattern="yyyy-MM-dd'T'HH:mm"/>
                        <fmt:formatDate value="${date}" pattern="dd/MM/yyyy HH:mm"/>
                    </td>
                    <s:if test="%{!(#pari.vainqueur.equals('nul'))}">
                        <td>Victoire de <s:property value="#pari.vainqueur"/></td>
                    </s:if>
                    <s:else>
                        <td>Match nul</td>
                    </s:else>
                    <td>${pari.montant} €</td>
                    <td>
                        <s:if test="%{!(#pari.match.isCommenceOuFini())}">
                            <form id="formPariAnnule${pari.idPari}" method="post" action="/annuler">
                                <input type="hidden" name="idPari" value="${pari.idPari}">
                                <button type="submit" class="btn btn-danger" title="Annuler ce pari">
                                    <i class="far fa-window-close"></i>
                                </button>
                            </form>
                        </s:if>
                        <s:else>
                            Match terminé !
                        </s:else>
                    </td>
                </tr>
                <script>
                    $("document").ready(function () {
                        $("#formPariAnnule${pari.idPari}").submit(function (e) {
                            e.preventDefault();
                            $.ajax({
                                type: "post",
                                url: "/annuler",
                                data: $(this).serialize(),
                                success: function () {
                                    Swal.fire({
                                        icon: "success",
                                        title: 'Pari annulé',
                                        timer: 1500,
                                        timerProgressBar: true,
                                        showConfirmButton: false
                                    }).then((result) => {
                                        if (result.dismiss === Swal.DismissReason.timer) {
                                            window.location.href = "/gotomesparis";
                                        }
                                    })
                                }
                            })
                        })
                    });
                </script>
            </s:iterator>
            </tbody>
        </table>
    </div>
</main>
<%@ include file="../fragments/Footer.jsp" %>
