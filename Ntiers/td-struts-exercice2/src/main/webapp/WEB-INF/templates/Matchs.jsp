<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: mikes
  Date: 15/01/2022
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../fragments/Header.jsp" %>
<main class="pt-3">
    <div class="mx-auto fit-content">
        <h1>Liste des matchs</h1>
    </div>
    <div class="container table-responsive py-3">
        <table class="table table-bordered table-striped table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">Sport</th>
                <th scope="col">Equipe 1</th>
                <th scope="col">Equipe 2</th>
                <th scope="col">Date</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <s:iterator value="matchs" var="match">
                <tr>
                    <td><s:property value="#match.sport"/></td>
                    <td><s:property value="#match.equipe1"/></td>
                    <td><s:property value="#match.equipe2"/></td>
                    <td>
                        <fmt:parseDate var="date" value="${match.quand}" pattern="yyyy-MM-dd'T'HH:mm"/>
                        <fmt:formatDate value="${date}" pattern="dd/MM/yyyy HH:mm"/>
                    </td>
                    <td>
                        <s:if test="%{!(#match.isCommenceOuFini())}">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#match${match.idMatch}" title="Parier sur ce match">
                                <i class="fas fa-coins"></i>
                            </button>
                        </s:if>
                        <s:else>
                            Match terminé !
                        </s:else>
                    </td>
                    <!-- Modal -->
                    <div class="modal fade" id="match${match.idMatch}" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Soumettre un pari</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form id="formMatch${match.idMatch}" method="post" action="/parier">
                                        <input type="hidden" name="idMatch" value="${match.idMatch}">
                                        <div class="input-group mx-auto my-3 fit-content">
                                            <span class="input-group-text bg-dark text-light" id="sport">Sport</span>
                                            <input type="text" class="form-control" name="sport" aria-label="Sport"
                                                   aria-describedby="sport" value="${match.sport}" disabled>
                                        </div>
                                        <div class="input-group my-3">
                                            <span class="input-group-text bg-dark text-light"
                                                  id="equipe1">Equipe 1</span>
                                            <input type="text" class="form-control" name="equipe1" aria-label="Equipe1"
                                                   aria-describedby="equipe1" value="${match.equipe1}" disabled>
                                            <span class="input-group-text bg-dark text-light">VS</span>
                                            <input type="text" class="form-control" name="equipe2" aria-label="Equipe2"
                                                   aria-describedby="equipe2" value="${match.equipe2}" disabled>
                                            <span class="input-group-text bg-dark text-light"
                                                  id="equipe2">Equipe 2</span>
                                        </div>
                                        <div class="input-group mx-auto my-3 fit-content">
                                            <span class="input-group-text bg-dark text-light" id="date">Date</span>
                                            <input type="text" class="form-control" name="date" aria-label="Date"
                                                   aria-describedby="date"
                                                   value="<fmt:formatDate value="${date}" pattern="dd/MM/yyyy HH:mm"/>"
                                                   disabled>
                                        </div>
                                        <div class="input-group my-3">
                                            <span class="input-group-text bg-dark text-light"
                                                  id="vainqueur">Vainqueur</span>
                                            <select class="input-select" name="vainqueur" aria-label="Vainqueur"
                                                    aria-describedby="vainqueur">
                                                <option value="${match.equipe1}">${match.equipe1}</option>
                                                <option value="${match.equipe2}">${match.equipe2}</option>
                                                <option value="nul">Nul</option>
                                            </select>
                                            <span class="input-group-text bg-dark text-light"
                                                  id="mise">Mise (€)</span>
                                            <input type="number" class="form-control" name="mise" aria-label="Mise"
                                                   aria-describedby="mise" value="1" min="1" step="0.01">
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler
                                    </button>
                                    <button type="submit" form="formMatch${match.idMatch}" class="btn btn-primary">
                                        Parier
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </tr>
                <script>
                    $("document").ready(function () {
                        $("#formMatch${match.idMatch}").submit(function (e) {
                            e.preventDefault();
                            $.ajax({
                                type: "post",
                                url: "/parier",
                                data: $(this).serialize(),
                                success: function () {
                                    Swal.fire({
                                        icon: "success",
                                        title: 'Pari enregistré',
                                        timer: 1500,
                                        timerProgressBar: true,
                                        showConfirmButton: false
                                    }).then((result) => {
                                        if (result.dismiss === Swal.DismissReason.timer) {
                                            window.location.href = "/gotomatchs";
                                        }
                                    })
                                }
                            })
                        })
                    });
                </script>
            </s:iterator>
            </tbody>
        </table>
    </div>
</main>
<%@ include file="../fragments/Footer.jsp" %>
