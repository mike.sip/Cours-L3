#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Exemple d'utilisation de msocket et simpleroute, ensemble.
Agrège les réseaux connus d'un bloc et les envoie périodiquement.
"""

import asyncio
import time
from msocket import msocket
from socket import gethostname
from simpleroute import *


def mastr(tablerip, eif):
    """transforme 'tablerip' en une chaîne de caractères"""
    l = []
    for net in tablerip:
        if tablerip[net][2] == eif:
            l.append("%s: %d:" % (net, 16))
        else:
            l.append("%s: %d" % (net, tablerip[net][0]))
    return "\n".join(l)


def maparse(tablerip, s, rt, eif):
    """parse la chaîne de caractère 's' et met à jour 'tablerip'"""
    ss = s.strip().split("\n")
    print("Parsing '%s'" % (ss[0],))
    for l in ss[1:]:
        [net, hop] = l.strip().split(": ")
        if net in tablerip:
            tablerip[net] = [hop + 1, rt, eif, time.time()]
        else:
            if hop + 1 < tablerip[net][0] or rt == tablerip[net][1]:
                tablerip[net] = hop + 1, rt, eif, time.time()


async def send_tablerip(msocks, tablerip):
    """envoie la map aux sockets de 'msocks' toutes les 5 secondes"""
    host = gethostname()
    while True:
        print("Envoi de la map aux voisins :")
        s = mastr(tablerip, ifname())
        for ms in msocks:
            print("\t - Envoi sur %s" % (ms.ifname,))
            ms.msend(("de %s vers %s\n" %
                     (host, ms.ifname) + s).encode("utf-8"))
        await asyncio.sleep(5)


async def recv_tablerip(msock, tablerip):
    """réception des données et mise à jour de la map"""
    ch = "*****  Map actuelle  *****"
    while True:
        data, qui = await msock.mrecv()
        print("Reçu des données de %s via %s" % (qui[0], msock.ifname))
        maparse(tablerip, data.decode("utf-8"))
        print(ch, mastr(tablerip), "*" * len(ch), sep="\n")


async def rip_server(mgrp, mport, bloc):
    """serveur mmap"""

    # récupère le nom du routeur et les réseaux auxquels il est connecté
    host = gethostname()
    conet = [(net, eif) for (net, gw, eif) in getroutes() if gw is None]

    # crée une msocket pour chaque réseau connecté et initialise mmap avec les réseaux de [bloc]
    tablerip = {}
    msocks = []

    for (net, eif) in conet:
        mip = getaddr(eif)
        if addrinnet(mip, bloc):
            tablerip[net] = [0, None, eif, time.time()]
        print((net, eif, mip))

        ms = msocket(mgrp, mport, mip, eif)
        await ms.create_socket()

        ms.ifname = eif
        msocks.append(ms)

    print("Bonjour, j'ai %d interfaces et ma table rip courante est :" %
          (len(msocks),))
    print(mastr(tablerip))

    # envoi périodique de la map sur toutes les interfaces :
    send_task = asyncio.create_task(send_tablerip(msocks, tablerip))

    # réception des données :
    recv_tasks = []
    for msock in msocks:
        recv_tasks.append(asyncio.create_task(recv_tablerip(msock, tablerip)))

    # attente que toutes les tâches soient terminées
    aws = [send_task] + recv_tasks
    await asyncio.gather(*aws)


if __name__ == "__main__":
    mgrp = "239.0.0.54"
    mport = 5454
    bloc = "192.168.0.0/16"

    loop = asyncio.get_event_loop()
    loop.create_task(rip_server(mgrp, mport, bloc))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print("\nAu revoir !")
