#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import asyncio
import random

# Partie Log


def log_recu(addr, message):
    fichier = open("/shared/shifumi.log", "a")
    fichier.write("<<< " + addr + ": " +
                  time.strftime('%d/%m/%Y %H:%M:%S') + " -> " + message)
    fichier.close()


def log_envoye(addr, message):
    fichier = open("/shared/shifumi.log", "a")
    fichier.write(">>> " + addr + ": " +
                  time.strftime('%d/%m/%Y %H:%M:%S') + " -> " + message)
    fichier.close()

# Règle Shifumi


coups_shifumi = ["Rock", "Paper", "Scissors"]


def shifumi(coup_j1, coup_j2):
    res = None
    if coup_j1 == 0 and coup_j2 == 1:
        res = 1
    elif coup_j1 == 1 and coup_j2 == 2:
        res = 1
    elif coup_j1 == 2 and coup_j2 == 0:
        res = 1
    elif coup_j1 == 1 and coup_j2 == 0:
        res = -1
    elif coup_j1 == 2 and coup_j2 == 1:
        res = -1
    elif coup_j1 == 0 and coup_j2 == 2:
        res = -1
    else:
        res = 0
    return res


async def rcv_mode(addr, reader, writer):
    mode = await reader.readline()
    mode = mode.decode()
    log_recu(addr, mode)
    # On valide la reception du mode
    message = f"200: You choose the mode {mode[6]}\n"
    writer.write(message.encode())
    await writer.drain()
    log_envoye(addr, message)
    # On retourne le numéro du mode
    return mode[6]


async def rcv_nb_coups(addr, reader, writer):
    nb_coups = await reader.readline()
    nb_coups = nb_coups.decode()
    log_recu(addr, nb_coups)
    print("COUP", nb_coups[10:-2])
    # Si le nombre de coups n'est pas positif
    if not int(nb_coups[10:-2]) > 0:
        message = f"400: Please choose a positive number.\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
    else:
        # On valide la reception du nombre de coups
        message = f"200: You choose {nb_coups[10:-2]} rounds\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
        # On retourne le nombre de coups
        return int(nb_coups[10:-2])


async def rcv_coup(addr, reader, writer):
    coup = await reader.readline()
    coup = coup.decode()
    log_recu(addr, coup)
    # On retourne le coup joué par l'utilisateur
    return int(coup[6])

# Partie IA


def coup_random():
    coups_possibles = [0, 1, 2]
    return random.choice(coups_possibles)

# Partie namelesstable


compteur_sans_nom = 0
tables_sans_nom = {}
coups_sans_nom = {}


def creer_rejoindre_table(addr, k):
    global compteur_sans_nom
    res = False
    cle_table = None
    # On essaye de rejoindre une table dont le nombre de coups est identique à la demande
    for num, params in tables_sans_nom.items():
        if params[1] == None and params[2] == k:
            params[1] = addr
            cle_table = num
            res = True
            break
    # Sinon on crée notre propre table
    if not res:
        tables_sans_nom[compteur_sans_nom] = [addr, None, k]
        coups_sans_nom[compteur_sans_nom] = [None, None]
        cle_table = compteur_sans_nom
        compteur_sans_nom += 1
    return cle_table


async def attente_adversaire(num_table):
    await asyncio.sleep(1)
    if (tables_sans_nom[num_table][1] != None):
        return True


async def attente_coups(num_table):
    await asyncio.sleep(1)
    if (None not in coups_sans_nom[num_table]):
        return True


async def nettoyer_coups(num_table):
    await asyncio.sleep(1)
    coups_sans_nom[num_table] = [None, None]


# Partie namedtable


tables_nom = {}
coups_nom = {}


async def rcv_nb_coups_nom(addr, reader, writer):
    nb_coups = await reader.readline()
    nb_coups = nb_coups.decode()
    log_recu(addr, nb_coups)
    # Si le nombre de coups n'est pas positif
    if not int(nb_coups[10:-2]) > 0:
        message = f"400: Please choose a positive number.\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
    else:
        # On valide la reception du nombre de coups
        message = f"202: You choose {nb_coups[10:-2]} rounds\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
        # On retourne le nombre de coups
        return int(nb_coups[10:-2])


async def rcv_nom_table(addr, reader, writer):
    nom_table = await reader.readline()
    nom_table = nom_table.decode()
    log_recu(addr, nom_table)
    # Si le nom est déjà pris ou si le nom est vide
    if nom_table[7:-2] == "":
        message = f"400: Empty name. Please choose one\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
    elif nom_table[7:-2] in tables_nom.keys():
        message = f"400: Name already taken. Please choose another one\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
    else:
        message = f"200: You choose the name {nom_table[7:-2]}\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
        # On retourne le nom de la table
        return nom_table[7:-2]


def creer_table_nom(nom, addr, k):
    tables_nom[nom] = [addr, None, k]
    coups_nom[nom] = [None, None]


async def rejoindre_table_nom(reader, writer, addr):
    nom_table = await reader.readline()
    nom_table = nom_table.decode()
    log_recu(addr, nom_table)
    # Si le nom n'existe pas ou si le nom est vide
    if nom_table[7:-2] == "":
        message = f"500: Table name invalid.\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
    elif nom_table[7:-2] not in tables_nom.keys():
        message = f"500: Table name not found.\n"
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
    else:
        message = f"201: You joined the game with success.\n"
        tables_nom[nom_table[7:-2]][1] = addr
        writer.write(message.encode())
        await writer.drain()
        log_envoye(addr, message)
        return nom_table[7:-2]


async def attente_adversaire_nom(nom_table):
    await asyncio.sleep(1)
    if (tables_nom[nom_table][1] != None):
        return True


async def attente_coups_nom(nom_table):
    await asyncio.sleep(1)
    if (None not in coups_nom[nom_table]):
        return True


async def nettoyer_coups_nom(nom_table):
    await asyncio.sleep(1)
    coups_nom[nom_table] = [None, None]


async def handle_request(reader, writer):
    addr = writer.get_extra_info('peername')[0]

    # On valide la connexion avec la client
    writer.write(f"200: [0, 1, 2, 3]\n".encode())
    await writer.drain()
    log_envoye(addr, f"200: [0, 1, 2, 3]\n")

    message = f"User {addr} is connected."
    print(message)

    score_j1 = 0
    score_j2 = 0

    # Gestion des modes
    mode = await rcv_mode(addr, reader, writer)

    # Gestion du mode 0
    if mode == "0":
        # On attend le nombre de coups
        k = None
        while k == None:
            k = await rcv_nb_coups(addr, reader, writer)

        writer.write(f"{k + 300}: 0 - 0\n".encode())
        await writer.drain()
        log_envoye(addr, f"{k + 300}: 0 - 0\n")

        # Tant qu'il reste des manches
        while k > 0:
            # On récupère les coups des deux joueurs
            coup_ia = coup_random()
            coup_joueur = await rcv_coup(addr, reader, writer)

            # On compare les coups
            resultat = shifumi(coup_ia, coup_joueur)
            if resultat == -1:
                score_j1 = score_j1 + 1
                k = k - 1
            if resultat == 1:
                score_j2 = score_j2 + 1
                k = k - 1

            # On met à jour la partie
            if k == 0:
                if score_j1 > score_j2:
                    message = "You loose!"
                else:
                    message = "You win!"
                writer.write(
                    f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_ia]}. {message}\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_ia]}. {message}\n")
            else:
                writer.write(
                    f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_ia]}.\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_ia]}.\n")

    # Gestion du mode 1
    if mode == "1":
        # On attend le nombre de coups
        k = None
        while k == None:
            k = await rcv_nb_coups(addr, reader, writer)

        # On crée ou rejoint une table
        num_table = creer_rejoindre_table(addr, k)

        num_joueur = None

        if (tables_sans_nom[num_table][1] == None):
            writer.write(f"202: Waiting for an oponent.\n".encode())
            await writer.drain()
            log_envoye(addr, f"202: Waiting for an oponent.\n")
            adv = False
            while not adv:
                adv = await attente_adversaire(num_table)
        else:
            writer.write(f"201: Starting game now!\n".encode())
            await writer.drain()
            log_envoye(addr, f"201: Starting game now!\n")

        if (tables_sans_nom[num_table][0] == addr):
            num_joueur = 0
        else:
            num_joueur = 1

        writer.write(f"{k + 300}: 0 - 0\n".encode())
        await writer.drain()
        log_envoye(addr, f"{k + 300}: 0 - 0\n")

        # Tant qu'il reste des manches
        while k > 0:
            coup_joueur = await rcv_coup(addr, reader, writer)
            coups_sans_nom[num_table][num_joueur] = coup_joueur

            # On attend que les deux joueurs aient joués
            coup = False
            while not coup:
                coup = await attente_coups(num_table)
            coup_adversaire = coups_sans_nom[num_table][1 - num_joueur]

            # On compare les coups
            resultat = shifumi(coup_adversaire, coup_joueur)
            if resultat == -1:
                score_j1 += 1
                k = k - 1
            if resultat == 1:
                score_j2 += 1
                k = k - 1

            # On met à jour la partie
            if k == 0:
                if score_j1 > score_j2:
                    message = "You loose!"
                else:
                    message = "You win!"
                writer.write(
                    f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}. {message}\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}. {message}\n")
            else:
                writer.write(
                    f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}.\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}.\n")

            await nettoyer_coups(num_table)

    # Gestion du mode 2
    if mode == "2":
        # On attend le nom de la table
        nom_table = None
        while nom_table == None:
            nom_table = await rcv_nom_table(addr, reader, writer)

        # On attend le nombre de coups
        k = None
        while k == None:
            k = await rcv_nb_coups_nom(addr, reader, writer)

        creer_table_nom(nom_table, addr, k)

        num_joueur = 0

        adv = False
        while not adv:
            adv = await attente_adversaire_nom(nom_table)

        writer.write(f"{k + 300}: 0 - 0\n".encode())
        await writer.drain()
        log_envoye(addr, f"{k + 300}: 0 - 0\n")

        # Tant qu'il reste des manches
        while k > 0:
            coup_joueur = await rcv_coup(addr, reader, writer)
            coups_nom[nom_table][num_joueur] = coup_joueur

            # On attend que les deux joueurs aient joués
            coup = False
            while not coup:
                coup = await attente_coups_nom(nom_table)
            coup_adversaire = coups_nom[nom_table][1]

            # On compare les coups
            resultat = shifumi(coup_adversaire, coup_joueur)
            if resultat == -1:
                score_j1 += 1
                k = k - 1
            if resultat == 1:
                score_j2 += 1
                k = k - 1

            # On met à jour la partie
            if k == 0:
                if score_j1 > score_j2:
                    message = "You loose!"
                else:
                    message = "You win!"
                writer.write(
                    f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}. {message}\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}. {message}\n")
            else:
                writer.write(
                    f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}.\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}.\n")

            await nettoyer_coups_nom(nom_table)

    # Gestion du mode 3
    if mode == "3":
        # On attend le nom de la table à rejoindre
        nom_table = await rejoindre_table_nom(reader, writer, addr)

        num_joueur = 1

        k = tables_nom[nom_table][2]

        writer.write(f"{k + 300}: 0 - 0\n".encode())
        await writer.drain()
        log_envoye(addr, f"{k + 300}: 0 - 0\n")

        # Tant qu'il reste des manches
        while k > 0:
            coup_joueur = await rcv_coup(addr, reader, writer)
            coups_nom[nom_table][num_joueur] = coup_joueur

            # On attend que les deux joueurs aient joués
            coup = False
            while not coup:
                coup = await attente_coups_nom(nom_table)
            coup_adversaire = coups_nom[nom_table][0]

            # On compare les coups
            resultat = shifumi(coup_adversaire, coup_joueur)
            if resultat == -1:
                score_j1 += 1
                k = k - 1
            if resultat == 1:
                score_j2 += 1
                k = k - 1

            # On met à jour la partie
            if k == 0:
                if score_j1 > score_j2:
                    message = "You loose!"
                else:
                    message = "You win!"
                writer.write(
                    f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}. {message}\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"300: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}. {message}\n")
            else:
                writer.write(
                    f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}.\n".encode())
                await writer.drain()
                log_envoye(
                    addr, f"{k + 300}: {score_j2} - {score_j1} You played {coups_shifumi[coup_joueur]} against {coups_shifumi[coup_adversaire]}.\n")

            await nettoyer_coups_nom(nom_table)

    message = f"User {addr} quit the game."
    print(message)
    writer.close()


async def shifumi_server():
    server = await asyncio.start_server(handle_request, '0.0.0.0', 999)
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')
    async with server:
        await server.serve_forever()

if __name__ == '__main__':
    asyncio.run(shifumi_server())
