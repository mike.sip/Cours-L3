#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import argv
from socket import gethostbyname
import asyncio


async def smtp_client(server):
    # connecting to server on port 25
    reader, writer = await asyncio.open_connection(server, 25)
    data = await reader.readline()
    print(data.decode())

    writer.write("HELO delu\r\n".encode())
    data = await reader.readline()
    print(data.decode())


if __name__ == '__main__':
    if len(argv)!=2:
        print("usage: {scriptname} server".format(scriptname= argv[0]))
        exit(1)
    sname=argv[1]
    server=gethostbyname(sname)
    print("connecting to :", sname, server)
    asyncio.run(smtp_client(server))
