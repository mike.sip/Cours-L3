#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import asyncio

async def forward(reader, writer, addread, addrwri):
    while True:
        msg = await reader.readline()
        print(addread + " -> " + addrwri)
        writer.write(msg)

async def handle_request(readercli, writercli):
    addr = writercli.get_extra_info('peername')[0]

    message = f"User {addr} is connected to the proxy."
    print(message)
    readerServer, writerServer = await asyncio.open_connection(rhost, rport)
    task1 = asyncio.create_task(forward(readercli, writerServer, addr, rhost))
    task2 = asyncio.create_task(forward(readerServer, writercli, rhost, addr))
    await task1
    await task2

async def proxy_server():
    # start a socket server
    server = await asyncio.start_server(handle_request, '0.0.0.0', port)
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')
    async with server:
        await server.serve_forever() # handle requests for ever

if __name__ == '__main__':
    port = 8888
    rhost = "10.0.1.1"
    rport = 6666
    asyncio.run(proxy_server())
