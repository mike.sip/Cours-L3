#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import asyncio

async def handle_request(reader, writer):
    message = "Bonjour."
    print(message)
    writer.write(message.encode())
    writer.close()

async def datetime_server():
    # start a socket server
    server = await asyncio.start_server(handle_request, '0.0.0.0', 13)
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')
    async with server:
        await server.serve_forever() # handle requests for ever

if __name__ == '__main__':
    asyncio.run(datetime_server())
