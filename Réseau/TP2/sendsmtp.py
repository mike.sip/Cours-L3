#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import argv
from socket import gethostbyname
from datetime import date
import asyncio

async def send_email(server, destinataire, expediteur, sujet, message):
    # connecting to server on port 80
    reader, writer = await asyncio.open_connection(server, 25)

    message = "To : {destinaire}\r\nFrom : {expediteur}\r\nDate : {date}\r\nSubject : {sujet} \r\n {message}".format(expediteur=expediteur, destinataire=destinataire, date=date.today(), sujet=sujet, message=message)
    
    # string message is encoded to bytes (default encoding: utf-8)
    writer.write(message.encode())
    
    # get server's answer
    data = await reader.read()
    print(data.decode())
  

if __name__ == '__main__':
    if len(argv)!=2:
        print("usage: {scriptname} server".format(scriptname= argv[0]))
        exit(1)
    sname=argv[1]
    server=gethostbyname(sname)
    print("connecting to :", sname, server)
    asyncio.run(send_email(server))