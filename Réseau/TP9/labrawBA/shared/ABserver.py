#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from ABproto import *
from time import sleep

seq = 0

async def rcvsnd(s):
    """Reçoit un paquet AB et applique le protocole."""
    global seq
    dump, addr = await s.ABreceivefrom()
    p = ABpacket.fromstr(dump)
    if p.ABchk != 0:
        print("\r\n" + 30 * "*" + ("\nPaquet reçu de: %s\n" % addr[0]))
        p.affiche()  # demander un affichage complet avec p.affiche(full=True)
        sleep(0.5)
        p = ABpacket("ACK", 5555, 6666, ABtype=1)
        if s.ABsend(p.tostr(True), addr):
            print("\n" + 30 * "*" + "\r\nPaquet envoyé :\n")
            p.affiche()  # demander un affichage complet avec p.affiche(full=True)
        else:
            print("Échec lors de l'envoi.")
            return 0
    else:
        p = ABpacket("NACK", 5555, 6666, ABtype=2)
        if s.ABsend(p.tostr(True), addr):
            print("\n" + 30 * "*" + "\r\nPaquet envoyé :\n")
            p.affiche()  # demander un affichage complet avec p.affiche(full=True)
        else:
            print("Échec lors de l'envoi.")
            return 0

async def main():
    s = ABsocket()
    await s.create_ABsocket()
    # s.bufsize()  #Décommenter si on souhaite afficher les tailles des buffers
    while True:
        await rcvsnd(s)
    s.close()


if __name__ == "__main__":
    asyncio.run(main())
