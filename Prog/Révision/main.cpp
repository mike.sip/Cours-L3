#include <iostream>
#include <algorithm>
#include <string>
#include <map>

using namespace std;

class WrapInt
{
private:
    int n;

public:
    WrapInt(int _n)
    {
        this->n = _n;
    }
    WrapInt()
    {
        this->n = 0;
    }
    WrapInt(const WrapInt &wi)
    {
        cout << "copie" << endl;
    }
    void set_int(int _n)
    {
        this->n = _n;
    }
    friend ostream &operator<<(ostream &os, const WrapInt &wi);
};

ostream &operator<<(ostream &os, const WrapInt &wi)
{
    os << wi.n;
    return os;
}

template <class T>
class Tableau
{
private:
    T *t;
    int taille;
    int nb_elem;

public:
    Tableau(int _taille)
    {
        this->taille = _taille;
        this->t = new T[taille];
        nb_elem = 0;
    }
    void inserer(T val)
    {
        t[nb_elem] = val;
        nb_elem++;
    }
    T &operator[](int i) const
    {
        return this->t[i];
    }
    T &operator[](int i)
    {
        return this->t[i];
    }
    template <class U>
    friend ostream &operator<<(ostream &os, const Tableau<U> &tc);
};

template <class U>
ostream &operator<<(ostream &os, const Tableau<U> &tc)
{
    os << "[";
    for (int i = 0; i < tc.taille; i++)
    {
        os << tc[i] << ",";
    }
    os << "]";
    return os;
}

template <class T>
void fonction(T a)
{
    cout << "Un affichage " << endl;
}

template <>
void fonction(int a)
{
    cout << "Un autre affichage " << endl;
}

map<char, int> nb_occurence(string &s)
{
    map<char, int> map_occurence;
    for (int i = 0; i < s.size(); i++)
    {
        if (map_occurence.find(s[i]) == map_occurence.end())
        {

            map_occurence[s[i]] = 1;
        }
        else
        {
            map_occurence[s[i]] += 1;
        }
    }
    return map_occurence;
}

int main()
{
    string s = "fsdaazesdqfgsdgsqs";

    map<char, int> res = nb_occurence(s);

    for (map<char, int>::iterator it = res.begin(); it != res.end(); it++)
    {
        cout << it->first << " " << it->second << endl;
    }
    return 0;
}
