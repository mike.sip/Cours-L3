import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { AuthorsComponent } from './authors/authors.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BooksComponent } from './books/books.component';
import { AuthorComponent } from './author/author.component';
import { BookComponent } from './book/book.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EditAuthorComponent } from './edit-author/edit-author.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { MarkdownPipe } from './markdown.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AuthorsComponent,
    BooksComponent,
    AuthorComponent,
    BookComponent,
    EditAuthorComponent,
    EditBookComponent,
    MarkdownPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
