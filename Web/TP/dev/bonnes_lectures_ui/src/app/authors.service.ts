import {Injectable} from '@angular/core';
import {Author} from "./entity/Author";
import {delay} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  private authors: Map<bigint, Author> = new Map()
    .set(BigInt(-1), {id: BigInt(-1), firstName: "Jean", lastName: "Paul"})
    .set(BigInt(-2), {id: BigInt(-2), firstName: "Fred", lastName: "Eux"});

  constructor() {
  }

  public all(): Map<bigint, Author> {
    delay(5000);
    return this.authors;
  }

  public authorFromId(id: bigint): Author | undefined {
    return this.authors.get(id);
  }

  public deleteAuthorFromId(id: bigint): boolean {
    return this.authors.delete(id);
  }

  public createAuthor(author: Author): void {
    let newAuthor: Author = {
      firstName: author.firstName,
      lastName: author.lastName
    }
    this.authors.set(AuthorsService.maximum(this.authors.keys()) + BigInt(1), newAuthor);
  }

  public modifyAuthor(id: bigint, author: Author): void {
    this.authors.set(id, author);
  }

  private static maximum(array: IterableIterator<bigint>): bigint {
    let max: bigint = BigInt(0);
    for (let n of array) {
      if (n > max) max = n;
    }
    return max;
  }
}
