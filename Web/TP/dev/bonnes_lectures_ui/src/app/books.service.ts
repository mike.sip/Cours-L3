import {Injectable} from '@angular/core';
import {Book} from "./entity/Book";
import {delay} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  private books: Map<bigint, Book> = new Map()
    .set(BigInt(9781434216243), {
      title: "Snow Trouble",
      publisher: "Stone Arch Books",
      year: "2010",
      back_cover: "Livre de voiture",
      imageUrl: "https://covers.openlibrary.org/b/id/8209599-L.jpg"
    });

  constructor() {
  }

  public all(): Map<bigint, Book> {
    delay(5000);
    return this.books;
  }

  public bookFromId(id: bigint): Book | undefined {
    return this.books.get(id);
  }

  public deleteBookFromId(id: bigint): boolean {
    return this.books.delete(id);
  }

  public createBook(book: Book): void {
    let newBook: Book = {
      title: book.title,
      publisher: book.publisher,
      year: book.year,
      isbn: book.isbn,
      back_cover: book.back_cover,
      imageUrl: book.imageUrl
    };
    this.books.set(BooksService.maximum(this.books.keys()) + BigInt(1), newBook);
  }

  public modifyBook(id: bigint, book: Book): void {
    this.books.set(id, book);
  }

  private static maximum(array: IterableIterator<bigint>): bigint {
    let max: bigint = BigInt(0);
    for (let n of array) {
      if (n > max) max = n;
    }
    return max;
  }
}
