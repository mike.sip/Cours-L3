import {Component, OnInit} from '@angular/core';
import {Author} from "../entity/Author";
import {AuthorsService} from "../authors.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {
  id: bigint | undefined = undefined;
  author: Author | undefined = undefined;

  constructor(public authorsService: AuthorsService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  goBack(): void {
    this.location.back();
  }

  remove() {
    if (this.id !== undefined) {
      this.authorsService.deleteAuthorFromId(this.id);
    }
    this.goBack();
  }

  ngOnInit(): void {
    const idParameter: string | null = this.route.snapshot.paramMap.get('id');
    if (idParameter !== null) {
      this.id = BigInt(idParameter);
      this.author = this.authorsService.authorFromId(this.id);
    }
  }
}
