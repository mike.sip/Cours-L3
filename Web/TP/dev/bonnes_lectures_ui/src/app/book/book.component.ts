import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {Book} from "../entity/Book";
import {BooksService} from "../books.service";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  id: bigint | undefined = undefined;
  book: Book | undefined = undefined;

  constructor(public booksService: BooksService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  goBack(): void {
    this.location.back();
  }

  remove() {
    if (this.id !== undefined) {
      this.booksService.deleteBookFromId(this.id);
    }
    this.goBack();
  }

  ngOnInit(): void {
    const idParameter: string | null = this.route.snapshot.paramMap.get('id');
    if (idParameter !== null) {
      this.id = BigInt(idParameter);
      this.book = this.booksService.bookFromId(this.id);
    }
  }

}
