export interface Book {
  title: string;
  publisher: string;
  year: string;
  isbn: bigint;
  back_cover: string;
  imageUrl: string;
}
