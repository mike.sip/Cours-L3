import {Component, OnInit} from '@angular/core';
import {Author} from "../entity/Author";
import {AuthorsService} from "../authors.service";

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss']
})
export class AuthorsComponent implements OnInit {
  authors: Map<bigint, Author> = new Map();

  constructor(public authorsService: AuthorsService) {
  }

  ngOnInit(): void {
    this.authors = this.authorsService.all();
  }
}
