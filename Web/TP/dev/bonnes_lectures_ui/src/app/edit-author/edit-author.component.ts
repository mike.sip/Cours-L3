import {Component, OnInit} from '@angular/core';
import {Author} from "../entity/Author";
import {ActivatedRoute} from "@angular/router";
import {AuthorsService} from "../authors.service";
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.scss']
})
export class EditAuthorComponent implements OnInit {
  id: bigint | undefined = undefined;
  author: Author = {
    firstName: "",
    lastName: ""
  }

  constructor(private authorsService: AuthorsService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  goBack(): void {
    this.location.back();
  }

  private set(firstName: string, lastName: string): void {
    this.author.firstName = firstName;
    this.author.lastName = lastName;
  }

  create(firstName: string, lastName: string): void {
    this.set(firstName, lastName);
    this.authorsService.createAuthor(this.author);
    this.goBack();
  }

  update(firstName: string, lastName: string): void {
    if (this.id !== undefined) {
      this.set(firstName, lastName);
      this.authorsService.modifyAuthor(this.id, this.author);
      this.goBack();
    }
  }

  ngOnInit(): void {
    const idParameter: string | null = this.route.snapshot.paramMap.get('id');
    if (idParameter !== null) {
      this.id = BigInt(idParameter);
      const author = this.authorsService.authorFromId(this.id);
      if (author !== undefined) {
        this.author = author;
      }
    }
  }
}
