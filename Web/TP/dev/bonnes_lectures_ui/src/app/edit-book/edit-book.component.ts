import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {Book} from "../entity/Book";
import {BooksService} from "../books.service";

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {

  id: bigint | undefined = undefined;
  book: Book = {
    title: "",
    publisher: "",
    year: "",
    isbn: BigInt(0),
    back_cover: "",
    imageUrl: ""
  }

  constructor(private booksService: BooksService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  goBack(): void {
    this.location.back();
  }

  private set(title: string,
              publisher: string,
              year: string,
              back_cover: string,
              imageUrl: string): void {
    this.book.title = title;
    this.book.publisher = publisher;
    this.book.year = year;
    this.book.back_cover = back_cover;
    this.book.imageUrl = imageUrl;
  }

  create(title: string,
         publisher: string,
         year: string,
         back_cover: string,
         imageUrl: string): void {
    this.set(title, publisher, year, back_cover, imageUrl);
    this.booksService.createBook(this.book);
    this.goBack();
  }

  update(title: string,
         publisher: string,
         year: string,
         back_cover: string,
         imageUrl: string): void {
    if (this.id !== undefined) {
      this.set(title, publisher, year, back_cover, imageUrl);
      this.booksService.modifyBook(this.id, this.book);
      this.goBack();
    }
  }

  ngOnInit(): void {
    const idParameter: string | null = this.route.snapshot.paramMap.get('id');
    if (idParameter !== null) {
      this.id = BigInt(idParameter);
      const book = this.booksService.bookFromId(this.id);
      if (book !== undefined) {
        this.book = book;
      }
    }
  }

}
