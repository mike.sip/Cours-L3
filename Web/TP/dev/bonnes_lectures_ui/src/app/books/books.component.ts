import {Component, OnInit} from '@angular/core';
import {Book} from "../entity/Book";
import {BooksService} from "../books.service";

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books: Map<bigint, Book> = new Map();

  constructor(public booksService: BooksService) {
  }

  ngOnInit(): void {
    this.books = this.booksService.all();
  }

}
