import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorsComponent} from "./authors/authors.component";
import {BooksComponent} from "./books/books.component";
import {AuthorComponent} from "./author/author.component";
import {EditAuthorComponent} from "./edit-author/edit-author.component";
import {BookComponent} from "./book/book.component";
import {EditBookComponent} from "./edit-book/edit-book.component";

const routes: Routes = [
  {path: 'authors', component: AuthorsComponent},
  {path: 'author/:id', component: AuthorComponent},
  {path: 'edit-author', component: EditAuthorComponent},
  {path: 'edit-author/:id', component: EditAuthorComponent},
  {path: 'books', component: BooksComponent},
  {path: 'book/:id', component: BookComponent},
  {path: 'edit-book', component: EditBookComponent},
  {path: 'edit-book/:id', component: EditBookComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
